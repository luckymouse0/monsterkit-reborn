﻿using System.Collections.Generic;
using System.IO;

namespace Monsterkit_Reborn
{
    public static class IniFile
    {
        public static Dictionary<string, Dictionary<string, string>> ReadIniFile(string path)
        {
            Dictionary<string, Dictionary<string, string>> iniFile = new Dictionary<string, Dictionary<string, string>>();

            if (File.Exists(path))
            {
                string[] lines = File.ReadAllLines(path);
                string lastSection = "";
                foreach (var s in lines)
                {
                    if (s.StartsWith("[") && (s.EndsWith("]")))
                    {
                        lastSection = s.Substring(1, s.Length - 2);

                        if (!iniFile.ContainsKey(lastSection))
                        {
                            iniFile.Add(lastSection, new Dictionary<string, string>());
                        }
                    }
                    else if (!s.StartsWith(";") && s.Contains("="))
                    {
                        string[] split = s.Split('=');

                        if (!iniFile[lastSection].ContainsKey(split[0]))
                        {
                            iniFile[lastSection].Add(split[0], split[1]);
                        }
                    }
                }
            }

            return iniFile;
        }
    }
}

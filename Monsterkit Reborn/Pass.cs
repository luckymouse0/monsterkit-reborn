﻿using System.Collections.Generic;
using Microsoft.VisualBasic.FileIO;

namespace Monsterkit_Reborn
{
    public class PassFloor
    {
        public string Floor { get; set; }

        public string MName { get; set; }

        public string MElement { get; set; }

        public int MAttack { get; set; }

        public int MDefense { get; set; }

        public int MDexterity { get; set; }

        public int MLife { get; set; }

        public int MLevel { get; set; }

        public string Notes { get; set; }

        public PassFloor(
            string floor,
            string mname,
            string mElement,
            int mAttack,
            int mDefense,
            int mDexterity,
            int mLife,
            int mLevel,
            string notes)
        {
            Floor = floor;
            MName = mname;
            MElement = mElement;
            MAttack = mAttack;
            MDefense = mDefense;
            MDexterity = mDexterity;
            MLife = mLife;
            MLevel = mLevel;
            Notes = notes;
        }
    }

    public class Pass
    {
        List<PassFloor> passFloors;

        public Pass()
        {
            passFloors = new List<PassFloor>();
        }

        public List<PassFloor> GetPassFloors()
        {
            return passFloors;
        }

        public void ReadPass(MonsterCatalogue monsterCatalogue, string filename, string delimiter = ";")
        {
            using (TextFieldParser parser = new TextFieldParser(filename))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(delimiter);
                parser.ReadFields();  // reading the header

                while (!parser.EndOfData)
                {
                    // Processing rows
                    string[] fields = parser.ReadFields();

                    string floor = fields[0];
                    string mName = fields[1];
                    int mLevel = fields[2] != "" ? int.Parse(fields[2]) : 0;
                    string notes = fields.Length == 4 ? fields[3] : "";

                    Monster monster = monsterCatalogue.MonsterSearch(mName);

                    passFloors.Add(new PassFloor(
                        floor,
                        mName,
                        monster != null ? monster.Element : "",
                        monster != null ? monster.CalculateAttack(mLevel) : 0,
                        monster != null ? monster.CalculateDefense(mLevel) : 0,
                        monster != null ? monster.CalculateDexterity(mLevel) : 0,
                        monster != null ? monster.CalculateLife(mLevel) : 0,
                        mLevel,
                        notes));
                }
            }
        }
    }
}

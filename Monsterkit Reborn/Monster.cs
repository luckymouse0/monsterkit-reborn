﻿using System;
using System.Linq;

namespace Monsterkit_Reborn
{
    public class Monster
    {
        string element;
        int attackPercentage, defensePercentage, dexterityPercentage;
        float baseAttack, baseDefense, baseDexterity, baseLife, attGR, lifeGR;
        string type;

        public string Name { get; set; }

        public string Element
        {
            get { return element; }
            set
            {
                element = GlobalVariables.ELEMENTS.Contains(value) ? value : "";
            }
        }

        public float BaseAttack
        {
            get { return baseAttack; }
            set
            {
                baseAttack = value >= 1 && value <= GlobalVariables.MAX_STAT ? value : 0;
            }
        }

        public float BaseDefense
        {
            get { return baseDefense; }
            set
            {
                baseDefense = value >= 1 && value <= GlobalVariables.MAX_STAT ? value : 0;
            }
        }

        public float BaseDexterity
        {
            get { return baseDexterity; }
            set
            {
                baseDexterity = value >= 1 && value <= GlobalVariables.MAX_STAT ? value : 0;
            }
        }

        public float BaseLife
        {
            get { return baseLife; }
            set
            {
                baseLife = value >= 1 && value <= GlobalVariables.MAX_STAT ? value : 0;
            }
        }

        public float AttGR
        {
            get { return attGR; }
            set
            {
                attGR = value >= 0 && value <= GlobalVariables.MAX_STAT ? value : 0;
            }
        }

        public float LifeGR
        {
            get { return lifeGR; }
            set
            {
                lifeGR = value >= 0 && value <= GlobalVariables.MAX_STAT ? value : 0;
            }
        }
        
        public int AttackPercentage
        {
            get { return attackPercentage; }
            set
            {
                attackPercentage = value < 0 ? 0 : value > 100 ? 100 : value;
            }
        }

        public int DefensePercentage
        {
            get { return defensePercentage; }
            set
            {
                defensePercentage = value < 0 ? 0 : value > 100 ? 100 : value;
            }
        }

        public int DexterityPercentage
        {
            get { return dexterityPercentage; }
            set
            {
                dexterityPercentage = value < 0 ? 0 : value > 100 ? 100 : value;
            }
        }

        public string Type
        {
            get { return type; }
            set
            {
                type = GlobalVariables.MONSTER_TYPE.Contains(value) ? value : "";
            }
        }

        public Monster(
            string name,
            string element,
            float attack,
            float defense,
            float dexterity,
            float life,
            float attGR,
            float lifeGR,
            int attackPercentage,
            int defensePercentage,
            int dexterityPercentage,
            string type)
        {
            Name = name;
            Element = element;
            BaseAttack = attack;
            BaseDefense = defense;
            BaseDexterity = dexterity;
            BaseLife = life;
            AttGR = attGR;
            LifeGR = lifeGR;
            AttackPercentage = attackPercentage;
            DefensePercentage = defensePercentage;
            DexterityPercentage = dexterityPercentage;
            Type = type;
        }

        public int CalculateAttack(int level)
        {
            return (int)Math.Floor(baseAttack + (level-1) * (attGR * GlobalVariables.PERCENTAGE_RATIO) * ((float)attackPercentage / 100));
        }

        public int CalculateDefense(int level)
        {
            return (int)Math.Floor(baseDefense + (level - 1) * (attGR * GlobalVariables.PERCENTAGE_RATIO) * ((float)defensePercentage / 100));
        }        

        public int CalculateDexterity(int level)
        {
            return (int)Math.Floor(baseDexterity + (level - 1) * (attGR * GlobalVariables.PERCENTAGE_RATIO) * ((float)dexterityPercentage / 100));
        }       

        public int CalculateLife(int level)
        {
            return (int)Math.Floor(baseLife + (level - 1) * (lifeGR * GlobalVariables.PERCENTAGE_RATIO));
        }

        public int CalculateAttack(int level, float attGR, float attackPercentage)
        {
            return level <= 1 ? (int)baseAttack : (int)Math.Floor((level - 1) * attGR * (attackPercentage / 100));
        }

        public int CalculateDefense(int level, float attGR, float defensePercentage)
        {
            return level <= 1 ? (int)baseDefense : (int)Math.Floor((level - 1) * attGR * (defensePercentage / 100));
        }

        public int CalculateDexterity(int level, float attGR, float dexterityPercentage)
        {
            return level <= 1 ? (int)baseDexterity : (int)Math.Floor((level - 1) * attGR * (dexterityPercentage / 100));
        }

        public int CalculateLife(int level, float lifeGR)
        {
            return level <= 1 ? (int)baseLife : (int)Math.Floor((level - 1) * lifeGR);
        }
    }
}

﻿using System.Collections.Generic;
using System.Globalization;
using Microsoft.VisualBasic.FileIO;

namespace Monsterkit_Reborn
{
    public class MonsterCatalogue
    {
        List<Monster> catalogue;

        public MonsterCatalogue()
        {
            catalogue = new List<Monster>();
            LoadCatalogue();
        }

        public void LoadCatalogue(string filename=GlobalVariables.MONSTER_DB, string delimiter=",")
        {
            using (TextFieldParser parser = new TextFieldParser(filename))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(delimiter);
                parser.ReadFields();

                while (!parser.EndOfData)
                {
                    // Processing row
                    string[] fields = parser.ReadFields();

                    // Name,Element,Attack,Defense,Dexterity,Life,3-att GR,Life GR,Attack%,Defense%,Dexterity%,Type
                    monsterAdd(
                        fields[0],
                        fields[1],
                        float.Parse(fields[2], CultureInfo.InvariantCulture),
                        float.Parse(fields[3], CultureInfo.InvariantCulture),
                        float.Parse(fields[4], CultureInfo.InvariantCulture),
                        float.Parse(fields[5], CultureInfo.InvariantCulture),
                        float.Parse(fields[6], CultureInfo.InvariantCulture),
                        float.Parse(fields[7], CultureInfo.InvariantCulture),
                        int.Parse(fields[8]),
                        int.Parse(fields[9]),
                        int.Parse(fields[10]),
                        fields[11]);
                }
            }
        }

        private void monsterAdd(string name,
            string element,
            float attack,
            float defense,
            float dexterity,
            float life,
            float attGR,
            float lifeGR,
            int attackPercentage,
            int defensePercentage,
            int dexterityPercentage,
            string type)
        {
            catalogue.Add(new Monster(
                name,
                element,
                attack,
                defense,
                dexterity,
                life,
                attGR,
                lifeGR,
                attackPercentage,
                defensePercentage,
                dexterityPercentage,
                type));
        }

        public Monster MonsterSearch(string name)
        {
            Monster mon = null;

            foreach(Monster monster in catalogue)
            {
                if (monster.Name == name)
                {
                    mon = monster;
                    break;
                }
            }

            return mon;
        }

        public Monster GetMonster(int index)
        {
            return catalogue[index];
        }

        public int MonsterCount()
        {
            return catalogue.Count;
        }

        public Monster GetAverageMonster(int level, bool super = false)
        {
            int attack = 0;
            int defense = 0;
            int dexterity = 0;
            int life = 0;
            int count = 0;

            foreach (Monster monster in catalogue)
            {
                if (super)
                {
                    if (monster.Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.Super])
                    {
                        attack += monster.CalculateAttack(level);
                        defense += monster.CalculateDefense(level);
                        dexterity += monster.CalculateDexterity(level);
                        life += monster.CalculateLife(level);
                        count++;
                    }
                }
                else
                {
                    attack += monster.CalculateAttack(level);
                    defense += monster.CalculateDefense(level);
                    dexterity += monster.CalculateDexterity(level);
                    life += monster.CalculateLife(level);
                    count++;
                }
            }

            attack /= count;
            defense /= count;
            dexterity /= count;
            life /= count;

            return new Monster(
                super ? "Random Super" : "Random",
                "Random",
                attack,
                defense,
                dexterity,
                life,
                0,
                0,
                0,
                0,
                0,
                super ? GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.Super] : "");
        }

        public List<string> GetMonsterNames(string element = GlobalVariables.ANY_SEARCH)
        {
            List<string> monsterNames = new List<string>();

            for (int i = 0; i < MonsterCount(); i++)
            {
                if ((element == GlobalVariables.ANY_SEARCH) || (GetMonster(i).Element == element))
                {
                    monsterNames.Add(GetMonster(i).Name);
                }
            }
            monsterNames.Sort();

            return monsterNames;
        }
    }
}

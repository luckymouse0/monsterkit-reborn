﻿using System.Collections.Generic;
using Microsoft.VisualBasic.FileIO;

namespace Monsterkit_Reborn
{
    class Map
    {
        public string Zone { get; set; }

        public string Name { get; set; }

        public Map(string zone, string name)
        {
            Zone = zone;
            Name = name;
        }
    }

    class WorldMap
    {
        List<Map> worldMap;

        public WorldMap()
        {
            worldMap = new List<Map>();
            LoadWorldMap();
        }

        public List<Map> GetWorldMap()
        {
            return worldMap;
        }

        public void LoadWorldMap(string filename = GlobalVariables.MAP_DB, string delimiter = ";", string mapIniFilename = GlobalVariables.MAP_INI)
        {
            Dictionary<string, Dictionary<string, string>> mapini = IniFile.ReadIniFile("map.ini");
            
            if (mapini.Count > 0)
            {
                loadMapINI(mapini);
            }
            else
            {
                loadMapDB(filename, delimiter);
            }
        }

        public string GetZone(string name)
        {
            string zone = "";

            foreach (Map map in worldMap)
            {
                if (formatName(name) == formatName(map.Name))
                {
                    zone = map.Zone;
                    break;
                }
            }

            return zone;
        }

        public List<string> GetZones()
        {
            List<string> zones = new List<string>();

            foreach (Map map in worldMap)
            {
                if (!zones.Contains(map.Zone))
                {
                    zones.Add(map.Zone);
                }
            }

            return zones;
        }

        public List<string> GetMaps(string zone = GlobalVariables.ANY_SEARCH)
        {
            List<string> maps = new List<string>();

            foreach (Map map in worldMap)
            {
                if ((zone == GlobalVariables.ANY_SEARCH || map.Zone == zone) && !maps.Contains(map.Name))
                {
                    maps.Add(map.Name);
                }
            }

            return maps;
        }

        private void loadMapDB(string filename, string delimiter)
        {
            using (TextFieldParser parser = new TextFieldParser(filename))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(delimiter);
                parser.ReadFields();

                while (!parser.EndOfData)
                {
                    // Processing row
                    string[] fields = parser.ReadFields();
                    worldMap.Add(new Map(fields[0], fields[1]));
                }
            }
        }

        private void loadMapINI(Dictionary<string, Dictionary<string, string>> mapini)
        {
            // Get zones
            string[] zones = new string[int.Parse(mapini["zone"]["Num"])];
            for (int i = 0; i < zones.Length; i++)
            {
                zones[i] = mapini["zone"][$"Zone{i}"];
            }

            // Get maps
            int mapsCount = int.Parse(mapini["main"]["MapNum"]);
            for (int i = 0; i < mapsCount; i++)
            {
                if (mapini.ContainsKey($"map{i}")
                    && mapini[$"map{i}"].ContainsKey("Zone")
                    && mapini[$"map{i}"].ContainsKey("Name"))
                {
                    worldMap.Add(new Map(
                        zones[int.Parse(mapini[$"map{i}"]["Zone"])],
                        mapini[$"map{i}"]["Name"]));
                }
            }
        }

        private string formatName(string name)
        {
            return name
                .Replace(" ", "")
                .Replace("-", "")
                .Replace("(", "")
                .Replace(")", "")
                .Replace(".", "")
                .ToLower();
        }
    }
}

﻿using System.Drawing;

namespace Monsterkit_Reborn
{
    public static class GlobalVariables
    {
        public const string VERSION = "1.6.0";
        public const int MAX_LEVEL = 8000;
        public const int MAX_STAT = 999999;
        public const float PERCENTAGE_RATIO = 0.90f;  // adjustment to value calculations

        public const string WORLD_PATH = "./db/world/";
        public const string MONSTER_DB = WORLD_PATH + "monsterdb.csv";
        public const string MAP_DB = WORLD_PATH + "mapdb.csv";
        public const string ENCOUNTER_DB = WORLD_PATH + "encounterdb.csv";
        public const string MAP_INI = WORLD_PATH + "map.ini";
        public const string PASSES_PATH = "./db/passes/";
        public static readonly string[] PASSES_DB = {
            $"{PASSES_PATH}skypassdb.csv",
            $"{PASSES_PATH}friendpassdb.csv",
            $"{PASSES_PATH}couplepassdb.csv" };

        public static readonly string[] ELEMENTS = { "Water", "Fire", "Metal", "Wood", "Earth" }; // Water>Fire, Fire>Metal, Metal>Wood, Wood>Earth, Earth>Water
        public static readonly Color[] ELEMENTS_COLORS = { Color.SkyBlue, Color.Orange, Color.Silver, Color.YellowGreen, Color.Khaki };
        public static readonly Color[] EFFECTIVENESS_COLORS = { Color.LightGreen, Color.White, Color.Orange };
        public static readonly Color NEW_SPOTS_COLOR = Color.FromArgb(230, 230, 230);
        public const string ANY_SEARCH = "ANY";

        public enum Effectiveness
        {
            Strong,
            Neutral,
            Weak
        }

        public static readonly string[] MONSTER_TYPE = { "Non-Evo", "Evo", "Super" };

        public enum MonsterTypes
        {
            NonEvo,
            Evo,
            Super
        }
    }
}

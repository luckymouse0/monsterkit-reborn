﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace Monsterkit_Reborn
{
    public partial class Form1 : Form
    {
        MonsterCatalogue monsterCatalogue;
        WorldMap worldMap;

        public Form1()
        {
            InitializeComponent();

            Text = $"{Text} v{GlobalVariables.VERSION}";

            cb_pl.SelectedIndex = 0;
            cb_pl_type.SelectedIndex = 1;
            cb_passes_el.SelectedIndex = 0;
            cb_passes_p.SelectedIndex = 0;
            cb_passes_type.SelectedIndex = 1;

            monsterCatalogue = new MonsterCatalogue();

            worldMap = new WorldMap();
            worldMap.LoadWorldMap();

            cb_ms_mt.Items.Add(GlobalVariables.ANY_SEARCH);
            cb_ms_mt.Items.AddRange(GlobalVariables.ELEMENTS);
            cb_ms_mt.SelectedIndex = 0;

            cb_ms_zone.Items.Add(GlobalVariables.ANY_SEARCH);
            cb_ms_zone.Items.AddRange(worldMap.GetZones().ToArray());
            cb_ms_zone.SelectedIndex = 0;        

            List<string> monsterNames = monsterCatalogue.GetMonsterNames();
            cb_sc.Items.AddRange(monsterNames.ToArray());
            cb_sc.Text = monsterNames[0];
        }

        private void btn_pl_Click(object sender, EventArgs e)
        {
            List<Encounter> encounters = Encounter.ReadEncounters(monsterCatalogue);
            List<Encounter> validEncounters = new List<Encounter>();
            List<Encounter> nextEncounters = new List<Encounter>();
            int defense = int.Parse(tb_pl_def.Text);
            int dexterity = 0;
            string type = GlobalVariables.MONSTER_TYPE.Contains(cb_pl_type.Text) ? cb_pl_type.Text : "";

            if (chk_pl_dex.Checked)
            {
                dexterity = int.Parse(tb_pl_dex.Text);
            }

            foreach (Encounter encounter in encounters)
            {
                int dexterity1 = encounter.M1Dexterity;
                int dexterity2 = encounter.M2Dexterity;

                calculateStats(
                    new Monster(
                        "",
                        cb_pl.Text,
                        0,
                        defense,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        type),
                    encounter,
                    out int atk1,
                    out int def1,
                    out GlobalVariables.Effectiveness m1Effective,
                    out int atk2,
                    out int def2,
                    out GlobalVariables.Effectiveness m2Effective);

                if (def1 >= atk1 && def2 >= atk2)
                {
                    if (chk_pl_element.Checked)
                    {
                        if (m1Effective == GlobalVariables.Effectiveness.Strong
                            && m2Effective == GlobalVariables.Effectiveness.Strong)
                        {
                            if (chk_pl_dex.Checked)
                            {
                                if (dexterity > dexterity1 && dexterity > dexterity2)
                                {
                                    validEncounters.Add(encounter);
                                }
                            }
                            else
                            {
                                validEncounters.Add(encounter);
                            }
                        }
                    }
                    else if (chk_pl_element_avoid.Checked)
                    {
                        if (m1Effective != GlobalVariables.Effectiveness.Weak
                            && m2Effective != GlobalVariables.Effectiveness.Weak)
                        {
                            checkDexEncounters(
                                validEncounters,
                                dexterity,
                                encounter,
                                dexterity1,
                                dexterity2);
                        }
                    }
                    else
                    {
                        checkDexEncounters(
                            validEncounters,
                            dexterity,
                            encounter,
                            dexterity1,
                            dexterity2);
                    }
                }
                else
                {
                    if (chk_pl_next.Checked)
                    {
                        int statsDiff = 0;

                        if (atk1 > atk2)
                        {
                            if (atk1 > def1)
                            {
                                statsDiff += atk1 - def1;
                            }
                        }
                        else
                        {
                            if (atk2 > def2)
                            {
                                statsDiff += atk2 - def2;
                            }
                        }

                        if (chk_pl_dex.Checked)
                        {
                            if (dexterity1 > dexterity2)
                            {
                                if (dexterity1 > dexterity)
                                {
                                    statsDiff += dexterity1 - dexterity;
                                }
                            }
                            else
                            {
                                if (dexterity2 > dexterity)
                                {
                                    statsDiff += dexterity2 - dexterity;
                                }
                            }
                        }

                        addNewEncounters(nextEncounters, defense, dexterity, encounter, statsDiff, chk_pl_dex.Checked, false, type);
                    }
                }
            }

            int highestLevel = showGridData(validEncounters);

            int monsterShown = 0;

            if (chk_pl_next.Checked)
            {
                foreach (Encounter encounter in nextEncounters)
                {
                    if (encounter.M1Level > highestLevel || encounter.M2Level > highestLevel)
                    {
                        calculateStats(
                            new Monster(
                                "",
                                cb_pl.Text,
                                0,
                                defense,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                type),
                            encounter,
                            out int atk1,
                            out int def1,
                            out GlobalVariables.Effectiveness m1Effective,
                            out int atk2,
                            out int def2,
                            out GlobalVariables.Effectiveness m2Effective);

                        getStatsDif(
                            dexterity,
                            encounter,
                            atk1,
                            def1,
                            atk2,
                            def2,
                            out string attack1Dif,
                            out string attack2Dif,
                            out string dexterity1Dif,
                            out string dexterity2Dif,
                            chk_pl_dex.Checked);

                        if (monsterShown < int.Parse(tb_pl_next.Text))
                        {
                            bool addMonster = false;

                            if (chk_pl_element.Checked)
                            {
                                if (m1Effective == GlobalVariables.Effectiveness.Strong
                                    && m2Effective == GlobalVariables.Effectiveness.Strong)
                                {
                                    addMonster = true;
                                }
                            }
                            else if (chk_pl_element_avoid.Checked)
                            {
                                if (m1Effective != GlobalVariables.Effectiveness.Weak
                                    && m2Effective != GlobalVariables.Effectiveness.Weak)
                                {
                                    addMonster = true;
                                }
                            }
                            else
                            {
                                addMonster = true;
                            }

                            if (addMonster)
                            {
                                monsterShown = addMonsterRow(
                                    encounter,
                                    attack1Dif,
                                    attack2Dif,
                                    dexterity1Dif,
                                    dexterity2Dif,
                                    monsterShown);
                            }
                        }
                    }
                }
            }

            sortColorization(monsterShown, true);
        }

        private void addNewEncounters(
            List<Encounter> nextEncounters,
            int defense,
            int dexterity,
            Encounter encounter,
            int statsDiff,
            bool dexCHecked,
            bool player = false,
            string type = "")
        {
            if (nextEncounters.Count == 0)
            {
                nextEncounters.Add(encounter);
            }
            else
            {
                int index = -1;

                for (int i = 0; i < nextEncounters.Count; i++)
                {
                    int statsDiff2 = 0;

                    int atk1;
                    int atk2;
                    int def1 = defense;
                    int def2 = defense;

                    if (player)
                    {
                        atk1 = nextEncounters[i].M1Attack * 2;
                        atk2 = nextEncounters[i].M2Attack * 2;
                    }
                    else
                    {
                        calculateStats(
                            new Monster(
                                "",
                                cb_pl.Text,
                                0,
                                defense,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                type),
                            nextEncounters[i],
                            out atk1,
                            out def1,
                            out _,
                            out atk2,
                            out def2,
                            out _);
                    }

                    if (atk1 > atk2)
                    {
                        if (atk1 > def1)
                        {
                            statsDiff2 += atk1 - def1;
                        }
                    }
                    else
                    {
                        if (atk2 > def2)
                        {
                            statsDiff2 += atk2 - def2;
                        }
                    }

                    if (dexCHecked)
                    {
                        if (nextEncounters[i].M1Dexterity > nextEncounters[i].M2Dexterity)
                        {
                            if (nextEncounters[i].M1Dexterity > dexterity)
                            {
                                statsDiff2 += nextEncounters[i].M1Dexterity - dexterity;
                            }
                        }
                        else
                        {
                            if (nextEncounters[i].M2Dexterity > dexterity)
                            {
                                statsDiff2 += nextEncounters[i].M2Dexterity - dexterity;
                            }
                        }
                    }

                    if (statsDiff < statsDiff2)
                    {
                        index = i;
                        break;
                    }
                }

                if (index > -1)
                {
                    nextEncounters.Insert(index, encounter);
                }
                else
                {
                    nextEncounters.Add(encounter);
                }
            }
        }

        private void getStatsDif(
            int dexterity,
            Encounter encounter,
            int atk1,
            int def1,
            int atk2,
            int def2,
            out string attack1Dif,
            out string attack2Dif,
            out string dexterity1Dif,
            out string dexterity2Dif,
            bool dexChecked)
        {
            attack1Dif = "" + encounter.M1Attack;
            attack2Dif = "" + encounter.M2Attack;
            dexterity1Dif = "" + encounter.M1Dexterity;
            dexterity2Dif = "" + encounter.M2Dexterity;
            if (atk1 > def1)
            {
                attack1Dif += "(+" + (atk1 - def1) + " def)";
            }

            if (atk2 > def2)
            {
                attack2Dif += "(+" + (atk2 - def2) + " def)";
            }

            if (dexChecked)
            {
                if (encounter.M1Dexterity > dexterity)
                {
                    dexterity1Dif += "(+" + (encounter.M1Dexterity - dexterity) + " dex)";
                }

                if (encounter.M2Dexterity > dexterity)
                {
                    dexterity2Dif += "(+" + (encounter.M2Dexterity - dexterity) + " dex)";
                }
            }
        }

        private int addMonsterRow(
            Encounter encounter,
            string attack1Dif,
            string attack2Dif,
            string dexterity1Dif,
            string dexterity2Dif,
            int monsterShown)
        {
            dgv_encounters.Rows.Add(
                encounter.Map,
                encounter.Coords,
                encounter.M1Name,
                encounter.M1Level,
                attack1Dif,
                encounter.M1Defense,
                dexterity1Dif,
                encounter.M1Life,
                encounter.M1Element,
                encounter.M2Name,
                encounter.M2Level,
                attack2Dif,
                encounter.M2Defense,
                dexterity2Dif,
                encounter.M2Life,
                encounter.M2Element);
            return ++monsterShown;
        }

        private void sortColorization(int monsterShown, bool cellColor = false)
        {
            dgv_encounters.Sort(dgv_encounters.Columns[3], ListSortDirection.Descending);

            for (int i = 0; i < monsterShown; i++)
            {
                dgv_encounters.Rows[i].DefaultCellStyle.BackColor = GlobalVariables.NEW_SPOTS_COLOR;
            }

            if (cellColor && !radioButtonColorNone.Checked)
            {
                cellColorization();
            }
        }

        private void checkDexEncounters(
            List<Encounter> validEncounters,
            int dexterity,
            Encounter encounter,
            int dexterity1,
            int dexterity2)
        {
            if (chk_pl_dex.Checked)
            {
                if (dexterity > dexterity1 && dexterity > dexterity2)
                {
                    validEncounters.Add(encounter);
                }
            }
            else
            {
                validEncounters.Add(encounter);
            }
        }

        private int showGridData(List<Encounter> validEncounters)
        {
            dgv_encounters.Rows.Clear();

            int highestLevel = 0;

            foreach (Encounter encounter in validEncounters)
            {
                if (encounter.M1Level > highestLevel)
                {
                    highestLevel = encounter.M1Level;
                }

                if (encounter.M2Level > highestLevel)
                {
                    highestLevel = encounter.M2Level;
                }

                try
                {
                    dgv_encounters.Rows.Add(
                        encounter.Map,
                        encounter.Coords,
                        encounter.M1Name,
                        encounter.M1Level,
                        encounter.M1Attack,
                        encounter.M1Defense,
                        encounter.M1Dexterity,
                        encounter.M1Life,
                        encounter.M1Element,
                        encounter.M2Name,
                        encounter.M2Level,
                        encounter.M2Attack,
                        encounter.M2Defense,
                        encounter.M2Dexterity,
                        encounter.M2Life,
                        encounter.M2Element);
                }
                catch
                {
                    //ERROR
                }
            }

            return highestLevel;
        }

        private void btn_tl_Click(object sender, EventArgs e)
        {
            List<Encounter> encounters = Encounter.ReadEncounters(monsterCatalogue);
            List<Encounter> validEncounters = new List<Encounter>();
            List<Encounter> nextEncounters = new List<Encounter>();
            int defense = int.Parse(tb_tl_def.Text);
            int dexterity = 0;

            if (chk_tl_dex.Checked)
            {
                dexterity = int.Parse(tb_tl_dex.Text);
            }

            foreach (Encounter encounter in encounters)
            {
                int attack1 = encounter.M1Attack * 2;
                int attack2 = encounter.M2Attack * 2;
                int dexterity1 = encounter.M1Dexterity;
                int dexterity2 = encounter.M2Dexterity;

                if (defense >= attack1 && defense >= attack2)
                {
                    if (chk_tl_dex.Checked)
                    {
                        if (dexterity > dexterity1 && dexterity > dexterity2)
                        {
                            validEncounters.Add(encounter);
                        }
                    }
                    else
                    {
                        validEncounters.Add(encounter);
                    }
                }
                else
                {
                    if (chk_tl_next.Checked)
                    {
                        int statsDiff = 0;

                        if (attack1 > attack2)
                        {
                            if (attack1 > defense)
                            {
                                statsDiff += attack1 - defense;
                            }
                        }
                        else
                        {
                            if (attack2 > defense)
                            {
                                statsDiff += attack2 - defense;
                            }
                        }

                        if (chk_tl_dex.Checked)
                        {
                            if (dexterity1 > dexterity2)
                            {
                                if (dexterity1 > dexterity)
                                {
                                    statsDiff += dexterity1 - dexterity;
                                }
                            }
                            else
                            {
                                if (dexterity2 > dexterity)
                                {
                                    statsDiff += dexterity2 - dexterity;
                                }
                            }
                        }

                        addNewEncounters(nextEncounters, defense, dexterity, encounter, statsDiff, chk_tl_dex.Checked, true);
                    }
                }
            }

            int highestLevel = showGridData(validEncounters);

            int monsterShown = 0;

            if (chk_tl_next.Checked)
            {

                foreach (Encounter encounter in nextEncounters)
                {
                    if (encounter.M1Level > highestLevel || encounter.M2Level > highestLevel)
                    {
                        getStatsDif(
                            dexterity,
                            encounter,
                            encounter.M1Attack * 2,
                            defense,
                            encounter.M2Attack * 2,
                            defense,
                            out string attack1Dif,
                            out string attack2Dif,
                            out string dexterity1Dif,
                            out string dexterity2Dif,
                            chk_tl_dex.Checked);

                        if (monsterShown < int.Parse(tb_tl_next.Text))
                        {
                            monsterShown = addMonsterRow(
                                encounter,
                                attack1Dif,
                                attack2Dif,
                                dexterity1Dif,
                                dexterity2Dif,
                                monsterShown);
                        }
                    }
                }
            }

            sortColorization(monsterShown);
        }

        private void btn_ms_Click(object sender, EventArgs e)
        {
            string monsterName = (string)cb_ms_mn.SelectedItem;

            List<Encounter> encounters = Encounter.ReadEncounters(monsterCatalogue);
            List<Encounter> validEncounters = new List<Encounter>();

            foreach (Encounter encounter in encounters)
            {
                if ((monsterName == GlobalVariables.ANY_SEARCH && (cb_ms_mt.Text == GlobalVariables.ANY_SEARCH
                    || (!chk_ms.Checked && (encounter.M1Element == cb_ms_mt.Text || encounter.M2Element == cb_ms_mt.Text))
                    || (chk_ms.Checked && (encounter.M1Element == cb_ms_mt.Text && encounter.M2Element == cb_ms_mt.Text))))
                    || (encounter.M1Name == monsterName || encounter.M2Name == monsterName))
                {
                    if (((string)cb_ms_zone.SelectedItem == GlobalVariables.ANY_SEARCH 
                        || (string)cb_ms_zone.SelectedItem == worldMap.GetZone(encounter.Map))
                        && ((string)cb_ms_map.SelectedItem == GlobalVariables.ANY_SEARCH
                        || (string)cb_ms_map.SelectedItem == encounter.Map))
                    {
                        validEncounters.Add(encounter);
                    }
                }
            }

            showGridData(validEncounters);

            dgv_encounters.Sort(dgv_encounters.Columns[3], ListSortDirection.Descending);
        }

        private void btn_sc_Click(object sender, EventArgs e)
        {
            showStatCalculatorGR();

            string monsterName = (string)cb_sc.SelectedItem;
            Monster monster = monsterCatalogue.MonsterSearch(monsterName);

            if (int.Parse(tb_sc_level.Text) <= 1)
            {
                tb_sc_level.Text = "1";
            }

            int monsterLevel = int.Parse(tb_sc_level.Text);

            if (float.Parse(tb_sc_attgr.Text, CultureInfo.InvariantCulture) <= 0.0f)
            {
                tb_sc_attgr.Text = monster.AttGR.ToString("0.00", CultureInfo.InvariantCulture);
            }

            if (float.Parse(tb_sc_lifegr.Text, CultureInfo.InvariantCulture) <= 0.0f)
            {
                tb_sc_lifegr.Text = monster.LifeGR.ToString("0.00", CultureInfo.InvariantCulture);
            }

            if (float.Parse(tb_sc_attackpercentage.Text, CultureInfo.InvariantCulture) <= 0.0f || float.Parse(tb_sc_attackpercentage.Text, CultureInfo.InvariantCulture) >= 100)
            {
                tb_sc_attackpercentage.Text = monster.AttackPercentage.ToString("0.00", CultureInfo.InvariantCulture);
            }

            if (float.Parse(tb_sc_defensepercentage.Text, CultureInfo.InvariantCulture) <= 0.0f || float.Parse(tb_sc_defensepercentage.Text, CultureInfo.InvariantCulture) >= 100)
            {
                tb_sc_defensepercentage.Text = monster.DefensePercentage.ToString("0.00", CultureInfo.InvariantCulture);
            }

            if (float.Parse(tb_sc_dexteritypercentage.Text, CultureInfo.InvariantCulture) <= 0.0f || float.Parse(tb_sc_dexteritypercentage.Text, CultureInfo.InvariantCulture) >= 100)
            {
                tb_sc_dexteritypercentage.Text = monster.DexterityPercentage.ToString("0.00", CultureInfo.InvariantCulture);
            }

            int attack = monster.CalculateAttack(
                monsterLevel,
                float.Parse(tb_sc_attgr.Text, CultureInfo.InvariantCulture),
                float.Parse(tb_sc_attackpercentage.Text, CultureInfo.InvariantCulture));
            int defense = monster.CalculateDefense(
                monsterLevel,
                float.Parse(tb_sc_attgr.Text, CultureInfo.InvariantCulture),
                float.Parse(tb_sc_defensepercentage.Text, CultureInfo.InvariantCulture));
            int dexterity = monster.CalculateDexterity(
                monsterLevel,
                float.Parse(tb_sc_attgr.Text, CultureInfo.InvariantCulture),
                float.Parse(tb_sc_dexteritypercentage.Text, CultureInfo.InvariantCulture));
            int life = monster.CalculateLife(
                monsterLevel,
                float.Parse(tb_sc_lifegr.Text, CultureInfo.InvariantCulture));
            tb_sc_attack.Text = $"{attack}";
            tb_sc_defense.Text = $"{defense}";
            tb_sc_dexterity.Text = $"{dexterity}";
            tb_sc_life.Text = $"{life}";
        }

        private void showStatCalculatorGR()
        {
            label12.Visible = true;
            tb_sc_attgr.Visible = true;
            label13.Visible = true;
            tb_sc_lifegr.Visible = true;
            label14.Visible = true;
            tb_sc_attackpercentage.Visible = true;
            label15.Visible = true;
            tb_sc_defensepercentage.Visible = true;
            label16.Visible = true;
            tb_sc_dexteritypercentage.Visible = true;
        }

        private void btn_passes_Click(object sender, EventArgs e)
        {
            Pass pass = new Pass();
            pass.ReadPass(monsterCatalogue, GlobalVariables.PASSES_DB[cb_passes_p.SelectedIndex]);

            int playerDefense = int.Parse(tb_passes_tdef.Text);
            if (chk_passes_def.Checked)
            {
                playerDefense *= 2;
            }

            int petDefense = int.Parse(tb_passes_pdef.Text);
            int petDexterity = 0;
            if (chk_passes_dex.Checked)
            {
                petDexterity = int.Parse(tb_passes_pdex.Text);
            }
            string petType = GlobalVariables.MONSTER_TYPE.Contains(cb_passes_type.Text) ? cb_passes_type.Text : "";

            dgv_passes.Rows.Clear();

            foreach (PassFloor floor in pass.GetPassFloors())
            {
                try
                {
                    string enemyType = GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.Evo];

                    if (floor.MName == "_random" || floor.MName == "_super")
                    {
                        Monster random = monsterCatalogue.GetAverageMonster(floor.MLevel, floor.MName == "_super");
                        floor.MAttack = (int)random.BaseAttack;
                        floor.MDefense = (int)random.BaseDexterity;
                        floor.MDexterity = (int)random.BaseDexterity;
                        floor.MLife = (int)random.BaseLife;
                        floor.MName = random.Name;
                        floor.MElement = "Random";

                        if (floor.MName == "Random Super")
                        {
                            enemyType = GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.Super];
                        }
                    }
                    else
                    {
                        Monster mon = monsterCatalogue.MonsterSearch(floor.MName);

                        if (mon != null)
                        {
                            enemyType = mon.Type;
                        }
                    }

                    int enemyDexterity = floor.MDexterity;

                    calculateStats(
                    new Monster(
                        "",
                        cb_passes_el.Text,
                        0,
                        petDefense,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        petType),
                    new Encounter(
                        "",
                        "",
                        floor.MName,
                        floor.MLevel,
                        floor.MAttack,
                        floor.MDefense,
                        floor.MDexterity,
                        floor.MLife,
                        floor.MElement,
                        enemyType,
                        "(Alone)",
                        0,
                        0,
                        0,
                        0,
                        0,
                        "-1",
                        ""),
                    out int enemyAttack,
                    out int petDef,
                    out GlobalVariables.Effectiveness m1Effective,
                    out int _,
                    out int _,
                    out _);

                    int petDefenseDiff = 0;
                    if (enemyAttack > petDef)
                    {
                        petDefenseDiff += enemyAttack - petDef;
                    }

                    int playerDefenseDiff = 0;
                    if (enemyAttack > playerDefense)
                    {
                        playerDefenseDiff += enemyAttack - playerDefense;
                    }

                    int petDexterityDiff = 0;
                    if (chk_passes_dex.Checked)
                    {
                        if (enemyDexterity > petDexterity)
                        {
                            petDexterityDiff += enemyDexterity - petDexterity;
                        }
                    }

                    string floorAttack = "";
                    if (enemyAttack > 0)
                    {
                        floorAttack = $"{floor.MAttack}{(playerDefenseDiff > 0 ? $" (player +{playerDefenseDiff} def)" : "")}{(petDefenseDiff > 0 ? $" (pet +{petDefenseDiff} def)" : "")}";
                    }

                    string floorDexterity = "";
                    if (enemyDexterity > 0)
                    {
                        floorDexterity = $"{floor.MDexterity}{(petDexterityDiff > 0 ? $" (+{petDexterityDiff} dex)" : "")}";
                    }

                    string floorLevel = floor.MLevel != 0 ? floor.MLevel.ToString() : "";
                    string floorDefense = floor.MDefense != 0 ? floor.MDefense.ToString() : "";
                    string floorLife = floor.MLife != 0 ? floor.MLife.ToString() : "";

                    dgv_passes.Rows.Add(
                        floor.Floor,
                        floor.MName,
                        floorLevel,
                        floorAttack,
                        floorDefense,
                        floorDexterity,
                        floorLife,
                        floor.MElement,
                        floor.Notes);
                }
                catch
                {
                    //ERROR
                }
            }

            foreach (DataGridViewRow row in dgv_passes.Rows)
            {
                string rowText = row.Cells[3].Value.ToString() + row.Cells[5].Value.ToString();
                if (rowText.Contains("pet") || rowText.Contains("player"))
                {
                    row.DefaultCellStyle.BackColor = GlobalVariables.NEW_SPOTS_COLOR;
                }
            }
        }

        private GlobalVariables.Effectiveness getEffectivenessColor(int element1, int element2)
        {
            GlobalVariables.Effectiveness effective = GlobalVariables.Effectiveness.Neutral;

            if ((element1 == 0 && element2 == GlobalVariables.ELEMENTS.Length - 1)
                || (element1 == 0 && element2 == GlobalVariables.ELEMENTS.Length - 1)
                || (element2 == element1 - 1))
            {
                effective = GlobalVariables.Effectiveness.Weak;
            }
            else if ((element1 == GlobalVariables.ELEMENTS.Length - 1 && element2 == 0)
                || (element1 == GlobalVariables.ELEMENTS.Length - 1 && element2 == 0)
                || (element2 == element1 + 1))
            {
                effective = GlobalVariables.Effectiveness.Strong;
            }

            return effective;
        }

        private void calculateStats(
            Monster pet,
            Encounter enemies,
            out int atk1,
            out int def1,
            out GlobalVariables.Effectiveness m1Effective,
            out int atk2,
            out int def2,
            out GlobalVariables.Effectiveness m2Effective)
        {
            /* DMG CALCULATIONS:
               No Advantage                         atkA * 2 - defB
               Element A > Element B                atkA * 2 - defB / 2
               Element A < Element B                atkA * 2 - defB * 2
               Non-Evo A (Neutral) vs Super B       atkA * 2 - defB / 2
               Non-Evo A (Strong) vs Super B        atkA * 3 - defB / 2
               Super A vs Non-Evo B                 atkA * 2 - defB * 3
             */

            m1Effective = GlobalVariables.Effectiveness.Neutral;

            if (enemies.M1Element != "-1" && enemies.M1Element != "")
            {
                m1Effective = getEffectivenessColor(
                    Array.IndexOf(GlobalVariables.ELEMENTS, pet.Element),
                    Array.IndexOf(GlobalVariables.ELEMENTS, enemies.M1Element));
            }

            m2Effective = GlobalVariables.Effectiveness.Neutral;

            if (enemies.M2Element != "-1" && enemies.M2Element != "")
            {
                m2Effective = getEffectivenessColor(
                    Array.IndexOf(GlobalVariables.ELEMENTS, pet.Element),
                    Array.IndexOf(GlobalVariables.ELEMENTS, enemies.M2Element));
            }

            if (pet.Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.Super])
            {
                atk1 = enemies.M1Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.NonEvo]
                    && m1Effective == GlobalVariables.Effectiveness.Weak
                    ? enemies.M1Attack * 3
                    : enemies.M1Attack * 2;

                atk2 = enemies.M2Name != "(Alone)"
                    && enemies.M2Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.NonEvo]
                    && m2Effective == GlobalVariables.Effectiveness.Weak
                    ? enemies.M2Attack * 3
                    : enemies.M2Attack * 2;
            }
            else
            {
                atk1 = enemies.M1Attack * 2;
                atk2 = enemies.M2Attack * 2;
            }

            def1 = (int)pet.BaseDefense;
            def2 = (int)pet.BaseDefense;

            if (pet.Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.Super]
                && enemies.M1Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.NonEvo])
            {
                def1 /= 2;
            }
            else if (pet.Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.Super]
                && enemies.M2Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.NonEvo])
            {
                def2 /= 2;
            }
            else if (pet.Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.NonEvo]
                && enemies.M1Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.Super])
            {
                def1 *= 3;
            }
            else if (enemies.M2Name != "(Alone)"
                && pet.Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.NonEvo]
                && enemies.M2Type == GlobalVariables.MONSTER_TYPE[(int)GlobalVariables.MonsterTypes.Super])
            {
                def2 *= 3;
            }
            else
            {
                switch (m1Effective)
                {
                    case GlobalVariables.Effectiveness.Strong:
                        {
                            def1 *= 2;
                            break;
                        }
                    case GlobalVariables.Effectiveness.Weak:
                        {
                            def1 /= 2;
                            break;
                        }
                }

                switch (m2Effective)
                {
                    case GlobalVariables.Effectiveness.Strong:
                        {
                            def2 *= 2;
                            break;
                        }
                    case GlobalVariables.Effectiveness.Weak:
                        {
                            def2 /= 2;
                            break;
                        }
                }
            }
        }

        private void cellColorization()
        {
            for (int r = 0; r < dgv_encounters.RowCount; r++)
            {
                Color color1 = Color.White;
                Color color2 = Color.White;

                if (radioButtonColorElement.Checked)
                {
                    color1 = GlobalVariables.ELEMENTS_COLORS[Array.IndexOf(GlobalVariables.ELEMENTS, (string)dgv_encounters.Rows[r].Cells[8].Value)];

                    if (Array.IndexOf(GlobalVariables.ELEMENTS, (string)dgv_encounters.Rows[r].Cells[15].Value) != -1)
                    {
                        color2 = GlobalVariables.ELEMENTS_COLORS[Array.IndexOf(GlobalVariables.ELEMENTS, (string)dgv_encounters.Rows[r].Cells[15].Value)];
                    }
                }
                else if (radioButtonColorEffective.Checked)
                {
                    int petElement = Array.IndexOf(GlobalVariables.ELEMENTS, cb_pl.Text);
                    int m1Element = Array.IndexOf(GlobalVariables.ELEMENTS, (string)dgv_encounters.Rows[r].Cells[8].Value);
                    int m2Element = Array.IndexOf(GlobalVariables.ELEMENTS, (string)dgv_encounters.Rows[r].Cells[15].Value);
                    GlobalVariables.Effectiveness m1Effective = getEffectivenessColor(petElement, m1Element);
                    GlobalVariables.Effectiveness m2Effective = getEffectivenessColor(petElement, m2Element);

                    if (m1Effective == GlobalVariables.Effectiveness.Strong)
                    {
                        color1 = GlobalVariables.EFFECTIVENESS_COLORS[(int)GlobalVariables.Effectiveness.Strong];
                    }
                    else if (m1Effective == GlobalVariables.Effectiveness.Weak)
                    {
                        color1 = GlobalVariables.EFFECTIVENESS_COLORS[(int)GlobalVariables.Effectiveness.Weak];
                    }

                    if (m2Element != -1)
                    {
                        if (m2Effective == GlobalVariables.Effectiveness.Strong)
                        {
                            color2 = GlobalVariables.EFFECTIVENESS_COLORS[(int)GlobalVariables.Effectiveness.Strong];
                        }
                        else if (m2Effective == GlobalVariables.Effectiveness.Weak)
                        {
                            color2 = GlobalVariables.EFFECTIVENESS_COLORS[(int)GlobalVariables.Effectiveness.Weak];
                        }
                    }
                }

                for (int k = 2; k < 9; k++)
                {
                    dgv_encounters.Rows[r].Cells[k].Style.BackColor = color1;
                }

                if (color2 != Color.White)
                {
                    for (int k = 9; k < 16; k++)
                    {
                        dgv_encounters.Rows[r].Cells[k].Style.BackColor = color2;
                    }
                }
            }
        }

        private void cb_sc_SelectionChangeCommitted(object sender, EventArgs e)
        {
            showStatCalculatorGR();

            string monsterName = (string)cb_sc.SelectedItem;
            Monster monster = monsterCatalogue.MonsterSearch(monsterName);
            tb_sc_attgr.Text = monster.AttGR.ToString("0.00", CultureInfo.InvariantCulture);
            tb_sc_lifegr.Text = monster.LifeGR.ToString("0.00", CultureInfo.InvariantCulture);

            tb_sc_attackpercentage.Text = monster.AttackPercentage.ToString("0.00", CultureInfo.InvariantCulture);
            tb_sc_defensepercentage.Text = monster.DefensePercentage.ToString("0.00", CultureInfo.InvariantCulture);
            tb_sc_dexteritypercentage.Text = monster.DexterityPercentage.ToString("0.00", CultureInfo.InvariantCulture);
        }

        private void Form_SizeChanged(object sender, EventArgs e)
        {
            dgv_encounters.Size = new Size(ClientSize.Width - (740 - 716), ClientSize.Height - (352 - 171));
            dgv_passes.Size = new Size(ClientSize.Width - (740 - 716), ClientSize.Height - (352 - 171));
        }

        private void chk_pl_dex_CheckedChanged(object sender, EventArgs e)
        {
            tb_pl_dex.Enabled = chk_pl_dex.Checked;
        }

        private void chk_tl_dex_CheckedChanged(object sender, EventArgs e)
        {
            tb_tl_dex.Enabled = chk_tl_dex.Checked;
        }

        private void chk_pl_next_CheckedChanged(object sender, EventArgs e)
        {
            tb_pl_next.Enabled = chk_pl_next.Checked;
        }

        private void chk_tl_next_CheckedChanged(object sender, EventArgs e)
        {
            tb_tl_next.Enabled = chk_tl_next.Checked;
        }

        private void dgv_encounters_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (!chk_pl_next.Checked
                || (e.Column.Name != "dgv_encounters_m1attack"
                && e.Column.Name != "dgv_encounters_m1dexterity"
                && e.Column.Name != "dgv_encounters_m2attack"
                && e.Column.Name != "dgv_encounters_m2dexterity"))
            {
                return;
            }
            else
            {
                sortCellValues(e);
            }

            e.Handled = true;
        }

        private static void sortCellValues(DataGridViewSortCompareEventArgs e)
        {
            if (!int.TryParse(e.CellValue1.ToString().Split('(')[0], out int cellValue1))
            {
                cellValue1 = 0;
            }

            if (!int.TryParse(e.CellValue2.ToString().Split('(')[0], out int cellValue2))
            {
                cellValue2 = 0;
            }

            e.SortResult = cellValue1 < cellValue2 ? 1 : cellValue1 > cellValue2 ? -1 : 0;
        }

        private void cb_Leave(object sender, EventArgs e)
        {
            if (!(sender is ComboBox cbx)) return;
            int i;
            cbx.SelectedIndex = (i = cbx.FindString(cbx.Text)) >= 0 ? i : 0;
        }

        private void cb_ms_mt_SelectedIndexChanged(object sender, EventArgs e)
        {
            cb_ms_mn.Items.Clear();
            cb_ms_mn.Items.Add(GlobalVariables.ANY_SEARCH);
            List<string> monsterNames = monsterCatalogue.GetMonsterNames(cb_ms_mt.Text);
            monsterNames.Sort();
            cb_ms_mn.Items.AddRange(monsterNames.ToArray());
            cb_ms_mn.SelectedIndex = 0;
        }

        private void cb_ms_mn_SelectedIndexChanged(object sender, EventArgs e)
        {
            chk_ms.Visible = (cb_ms_mn.SelectedIndex == 0 && cb_ms_mt.SelectedIndex != 0);
            chk_ms.Checked = false;
        }

        private void cb_ms_zone_SelectedIndexChanged(object sender, EventArgs e)
        {
            cb_ms_map.Items.Clear();
            cb_ms_map.Items.Add(GlobalVariables.ANY_SEARCH);
            cb_ms_map.Items.AddRange(worldMap.GetMaps(cb_ms_zone.Text).ToArray());
            cb_ms_map.SelectedIndex = 0;
        }

        private void linkLabelReleases_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://gitlab.com/luckymouse0/monsterkit-reborn/-/releases");
        }

        private void linkLabelSource_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://gitlab.com/luckymouse0/monsterkit-reborn");
        }

        private void linkLabelIssue_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://gitlab.com/luckymouse0/monsterkit-reborn/-/issues/?sort=created_date&state=opened");
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SelectedTab = tabControl1.SelectedTab.Text.ToString();

            if (SelectedTab == "Passes")
            {
                dgv_encounters.Visible = false;
                dgv_passes.Visible = true;
            }
            else
            {
                dgv_encounters.Visible = true;
                dgv_passes.Visible = false;
            }
        }

        private void chk_passes_dex_CheckedChanged(object sender, EventArgs e)
        {
            tb_passes_pdex.Enabled = chk_passes_dex.Checked;
        }

        private void dgv_passes_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column.Name == "dgv_passes_name"
                || e.Column.Name == "dgv_passes_element"
                || e.Column.Name == "dgv_passes_notes")
            {
                return;
            }
            else
            {
                sortCellValues(e);
            }

            e.Handled = true;
        }

        private void dgv_encounters_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if ((e.ColumnIndex == dgv_encounters.Columns["dgv_encounters_map"].Index) && e.Value != null)
            {
                DataGridViewCell cell = dgv_encounters.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = worldMap.GetZone((string)e.Value);
            }
        }

        private void chk_pl_element_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_pl_element.Checked)
            {
                chk_pl_element_avoid.Checked = false;
            }
        }

        private void chk_pl_element_avoid_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_pl_element_avoid.Checked)
            {
                chk_pl_element.Checked = false;
            }
        }
    }
}

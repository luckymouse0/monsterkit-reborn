﻿using Microsoft.VisualBasic.FileIO;
using System.Collections.Generic;

namespace Monsterkit_Reborn
{
    class Encounter
    {
        string coords;

        public string Map { get; }

        public string Coords
        {
            get { return coords; }
            set
            {
                if (value.StartsWith("(") && value.EndsWith(")"))
                {
                    coords = value;
                }
                else
                {
                    coords = "-";
                }
            }
        }

        public string M1Name { get; }

        public string M1Element { get; }

        public int M1Attack { get; }

        public int M1Defense { get; }

        public int M1Dexterity { get; }

        public int M1Life { get; }

        public int M1Level { get; }

        public string M1Type { get; }

        public string M2Name { get; }

        public string M2Element { get; }

        public int M2Attack { get; }

        public int M2Defense { get; }

        public int M2Dexterity { get; }

        public int M2Life { get; }

        public int M2Level { get; }

        public string M2Type { get; }

        public Encounter(string map, string coords, string m1Name, int m1Level, int m1Attack, int m1Defense, int m1Dexterity, int m1Life, string m1Element, string m1Type, string m2Name, int m2Level, int m2Attack, int m2Defense, int m2Dexterity, int m2Life, string m2Element, string m2Type)
        {
            Map = map;
            Coords = coords;
            M1Name = m1Name;
            M1Level = m1Level;
            M1Attack = m1Attack;
            M1Defense = m1Defense;
            M1Dexterity = m1Dexterity;
            M1Life = m1Life;
            M1Element = m1Element;
            M1Type = m1Type;
            M2Name = m2Name;
            M2Level = m2Level;
            M2Attack = m2Attack;
            M2Defense = m2Defense;
            M2Dexterity = m2Dexterity;
            M2Life = m2Life;
            M2Element = m2Element;
            M2Type = m2Type;
        }

        public static List<Encounter> ReadEncounters(MonsterCatalogue monsterCatalogue, string filename = GlobalVariables.ENCOUNTER_DB, string delimiter = ";")
        {
            List<Encounter> encounters = new List<Encounter>();

            using (TextFieldParser parser = new TextFieldParser(filename))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(delimiter);
                parser.ReadFields();  // reading the header

                while (!parser.EndOfData)
                {
                    // Processing rows
                    string[] fields = parser.ReadFields();

                    string map = fields[0];
                    string coords = fields[1];
                    string m1Name = fields[2];
                    int m1Level = int.Parse(fields[3]);
                    string m2Name = fields[4];
                    int m2Level = m2Name == "(Alone)" ? 0 : int.Parse(fields[5]);

                    Monster monster1 = monsterCatalogue.MonsterSearch(m1Name);
                    int attack1 = monster1.CalculateAttack(m1Level);
                    int defense1 = monster1.CalculateDefense(m1Level);
                    int dexterity1 = monster1.CalculateDexterity(m1Level);
                    int life1 = monster1.CalculateLife(m1Level);
                    string element1 = monster1.Element;
                    string type1 = monster1.Type;

                    Monster monster2 = monsterCatalogue.MonsterSearch(m2Name);
                    int attack2 = 0;
                    int defense2 = 0;
                    int dexterity2 = 0;
                    int life2 = 0;
                    string element2 = "";
                    string type2 = "";

                    if (monster2 != null)
                    {
                        attack2 = monster2.CalculateAttack(m2Level);
                        defense2 = monster2.CalculateDefense(m2Level);
                        dexterity2 = monster2.CalculateDexterity(m2Level);
                        life2 = monster2.CalculateLife(m2Level);
                        element2 = monster2.Element;
                        type2 = monster2.Type;
                    }

                    Encounter encounter = m1Level > m2Level
                        ? new Encounter(
                            map,
                            coords,
                            m1Name,
                            m1Level,
                            attack1,
                            defense1,
                            dexterity1,
                            life1,
                            element1,
                            type1,
                            m2Name,
                            m2Level,
                            attack2,
                            defense2,
                            dexterity2,
                            life2,
                            element2,
                            type2)
                        : new Encounter(
                            map,
                            coords,
                            m2Name,
                            m2Level,
                            attack2,
                            defense2,
                            dexterity2,
                            life2,
                            element2,
                            type2,
                            m1Name,
                            m1Level,
                            attack1,
                            defense1,
                            dexterity1,
                            life1,
                            element1,
                            type1);
                    encounters.Add(encounter);
                }
            }

            return encounters;
        }
    }
}

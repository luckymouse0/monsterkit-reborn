﻿namespace Monsterkit_Reborn
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dgv_encounters = new System.Windows.Forms.DataGridView();
            this.dgv_encounters_map = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_coords = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m1name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m1level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m1attack = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m1defense = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m1dexterity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m1life = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m1element = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m2name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m2level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m2attack = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m2defense = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m2dexterity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m2life = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters_m2element = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cb_ms_zone = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cb_ms_map = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.chk_ms = new System.Windows.Forms.CheckBox();
            this.cb_ms_mt = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cb_ms_mn = new System.Windows.Forms.ComboBox();
            this.btn_ms = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.chk_tl_next = new System.Windows.Forms.CheckBox();
            this.tb_tl_next = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.chk_tl_dex = new System.Windows.Forms.CheckBox();
            this.tb_tl_dex = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tb_tl_def = new System.Windows.Forms.TextBox();
            this.btn_tl = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chk_pl_element = new System.Windows.Forms.CheckBox();
            this.chk_pl_element_avoid = new System.Windows.Forms.CheckBox();
            this.cb_pl_type = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonColorEffective = new System.Windows.Forms.RadioButton();
            this.radioButtonColorElement = new System.Windows.Forms.RadioButton();
            this.radioButtonColorNone = new System.Windows.Forms.RadioButton();
            this.label21 = new System.Windows.Forms.Label();
            this.chk_pl_next = new System.Windows.Forms.CheckBox();
            this.tb_pl_next = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.chk_pl_dex = new System.Windows.Forms.CheckBox();
            this.tb_pl_dex = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tb_pl_def = new System.Windows.Forms.TextBox();
            this.cb_pl = new System.Windows.Forms.ComboBox();
            this.btn_pl = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tb_sc_dexteritypercentage = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tb_sc_defensepercentage = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tb_sc_attackpercentage = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tb_sc_lifegr = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_sc_attgr = new System.Windows.Forms.TextBox();
            this.tb_sc_life = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_sc_dexterity = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_sc_defense = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_sc_attack = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_sc_level = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cb_sc = new System.Windows.Forms.ComboBox();
            this.btn_sc = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.cb_passes_type = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cb_passes_p = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.chk_passes_dex = new System.Windows.Forms.CheckBox();
            this.tb_passes_pdex = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tb_passes_pdef = new System.Windows.Forms.TextBox();
            this.cb_passes_el = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.btn_passes = new System.Windows.Forms.Button();
            this.chk_passes_def = new System.Windows.Forms.CheckBox();
            this.tb_passes_tdef = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.linkLabelReleases = new System.Windows.Forms.LinkLabel();
            this.linkLabelSource = new System.Windows.Forms.LinkLabel();
            this.linkLabelIssue = new System.Windows.Forms.LinkLabel();
            this.label23 = new System.Windows.Forms.Label();
            this.dgv_passes = new System.Windows.Forms.DataGridView();
            this.dgv_passes_floor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_passes_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_passes_level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_passes_attack = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_passes_defense = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_passes_dexterity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_passes_life = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_passes_element = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_passes_notes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_encounters)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_passes)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_encounters
            // 
            this.dgv_encounters.AllowUserToAddRows = false;
            this.dgv_encounters.AllowUserToDeleteRows = false;
            this.dgv_encounters.AllowUserToOrderColumns = true;
            this.dgv_encounters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_encounters.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_encounters_map,
            this.dgv_encounters_coords,
            this.dgv_encounters_m1name,
            this.dgv_encounters_m1level,
            this.dgv_encounters_m1attack,
            this.dgv_encounters_m1defense,
            this.dgv_encounters_m1dexterity,
            this.dgv_encounters_m1life,
            this.dgv_encounters_m1element,
            this.dgv_encounters_m2name,
            this.dgv_encounters_m2level,
            this.dgv_encounters_m2attack,
            this.dgv_encounters_m2defense,
            this.dgv_encounters_m2dexterity,
            this.dgv_encounters_m2life,
            this.dgv_encounters_m2element});
            this.dgv_encounters.Location = new System.Drawing.Point(4, 163);
            this.dgv_encounters.Name = "dgv_encounters";
            this.dgv_encounters.ReadOnly = true;
            this.dgv_encounters.RowHeadersVisible = false;
            this.dgv_encounters.Size = new System.Drawing.Size(716, 171);
            this.dgv_encounters.TabIndex = 1;
            this.dgv_encounters.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_encounters_CellFormatting);
            this.dgv_encounters.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.dgv_encounters_SortCompare);
            // 
            // dgv_encounters_map
            // 
            this.dgv_encounters_map.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_map.HeaderText = "Map";
            this.dgv_encounters_map.Name = "dgv_encounters_map";
            this.dgv_encounters_map.ReadOnly = true;
            this.dgv_encounters_map.Width = 53;
            // 
            // dgv_encounters_coords
            // 
            this.dgv_encounters_coords.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_coords.HeaderText = "Coords";
            this.dgv_encounters_coords.Name = "dgv_encounters_coords";
            this.dgv_encounters_coords.ReadOnly = true;
            this.dgv_encounters_coords.Width = 65;
            // 
            // dgv_encounters_m1name
            // 
            this.dgv_encounters_m1name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m1name.HeaderText = "Name";
            this.dgv_encounters_m1name.Name = "dgv_encounters_m1name";
            this.dgv_encounters_m1name.ReadOnly = true;
            this.dgv_encounters_m1name.Width = 60;
            // 
            // dgv_encounters_m1level
            // 
            this.dgv_encounters_m1level.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m1level.HeaderText = "Level";
            this.dgv_encounters_m1level.Name = "dgv_encounters_m1level";
            this.dgv_encounters_m1level.ReadOnly = true;
            this.dgv_encounters_m1level.Width = 58;
            // 
            // dgv_encounters_m1attack
            // 
            this.dgv_encounters_m1attack.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m1attack.HeaderText = "Attack";
            this.dgv_encounters_m1attack.Name = "dgv_encounters_m1attack";
            this.dgv_encounters_m1attack.ReadOnly = true;
            this.dgv_encounters_m1attack.Width = 63;
            // 
            // dgv_encounters_m1defense
            // 
            this.dgv_encounters_m1defense.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m1defense.HeaderText = "Defense";
            this.dgv_encounters_m1defense.Name = "dgv_encounters_m1defense";
            this.dgv_encounters_m1defense.ReadOnly = true;
            this.dgv_encounters_m1defense.Width = 72;
            // 
            // dgv_encounters_m1dexterity
            // 
            this.dgv_encounters_m1dexterity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m1dexterity.HeaderText = "Dexterity";
            this.dgv_encounters_m1dexterity.Name = "dgv_encounters_m1dexterity";
            this.dgv_encounters_m1dexterity.ReadOnly = true;
            this.dgv_encounters_m1dexterity.Width = 73;
            // 
            // dgv_encounters_m1life
            // 
            this.dgv_encounters_m1life.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m1life.HeaderText = "Life";
            this.dgv_encounters_m1life.Name = "dgv_encounters_m1life";
            this.dgv_encounters_m1life.ReadOnly = true;
            this.dgv_encounters_m1life.Width = 49;
            // 
            // dgv_encounters_m1element
            // 
            this.dgv_encounters_m1element.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m1element.HeaderText = "Element";
            this.dgv_encounters_m1element.Name = "dgv_encounters_m1element";
            this.dgv_encounters_m1element.ReadOnly = true;
            this.dgv_encounters_m1element.Width = 70;
            // 
            // dgv_encounters_m2name
            // 
            this.dgv_encounters_m2name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m2name.HeaderText = "Name";
            this.dgv_encounters_m2name.Name = "dgv_encounters_m2name";
            this.dgv_encounters_m2name.ReadOnly = true;
            this.dgv_encounters_m2name.Width = 60;
            // 
            // dgv_encounters_m2level
            // 
            this.dgv_encounters_m2level.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m2level.HeaderText = "Level";
            this.dgv_encounters_m2level.Name = "dgv_encounters_m2level";
            this.dgv_encounters_m2level.ReadOnly = true;
            this.dgv_encounters_m2level.Width = 58;
            // 
            // dgv_encounters_m2attack
            // 
            this.dgv_encounters_m2attack.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m2attack.HeaderText = "Attack";
            this.dgv_encounters_m2attack.Name = "dgv_encounters_m2attack";
            this.dgv_encounters_m2attack.ReadOnly = true;
            this.dgv_encounters_m2attack.Width = 63;
            // 
            // dgv_encounters_m2defense
            // 
            this.dgv_encounters_m2defense.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m2defense.HeaderText = "Defense";
            this.dgv_encounters_m2defense.Name = "dgv_encounters_m2defense";
            this.dgv_encounters_m2defense.ReadOnly = true;
            this.dgv_encounters_m2defense.Width = 72;
            // 
            // dgv_encounters_m2dexterity
            // 
            this.dgv_encounters_m2dexterity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m2dexterity.HeaderText = "Dexterity";
            this.dgv_encounters_m2dexterity.Name = "dgv_encounters_m2dexterity";
            this.dgv_encounters_m2dexterity.ReadOnly = true;
            this.dgv_encounters_m2dexterity.Width = 73;
            // 
            // dgv_encounters_m2life
            // 
            this.dgv_encounters_m2life.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m2life.HeaderText = "Life";
            this.dgv_encounters_m2life.Name = "dgv_encounters_m2life";
            this.dgv_encounters_m2life.ReadOnly = true;
            this.dgv_encounters_m2life.Width = 49;
            // 
            // dgv_encounters_m2element
            // 
            this.dgv_encounters_m2element.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_encounters_m2element.HeaderText = "Element";
            this.dgv_encounters_m2element.Name = "dgv_encounters_m2element";
            this.dgv_encounters_m2element.ReadOnly = true;
            this.dgv_encounters_m2element.Width = 70;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.label1.Location = new System.Drawing.Point(551, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 39);
            this.label1.TabIndex = 2;
            this.label1.Text = "Created by: luckymouse0\r\n\r\nInspired in Monsterkit by GiulianuB";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.cb_ms_zone);
            this.tabPage3.Controls.Add(this.label31);
            this.tabPage3.Controls.Add(this.cb_ms_map);
            this.tabPage3.Controls.Add(this.label32);
            this.tabPage3.Controls.Add(this.chk_ms);
            this.tabPage3.Controls.Add(this.cb_ms_mt);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.cb_ms_mn);
            this.tabPage3.Controls.Add(this.btn_ms);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(519, 137);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Monster Search";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // cb_ms_zone
            // 
            this.cb_ms_zone.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_ms_zone.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_ms_zone.BackColor = System.Drawing.SystemColors.Window;
            this.cb_ms_zone.FormattingEnabled = true;
            this.cb_ms_zone.Location = new System.Drawing.Point(397, 13);
            this.cb_ms_zone.Name = "cb_ms_zone";
            this.cb_ms_zone.Size = new System.Drawing.Size(90, 21);
            this.cb_ms_zone.TabIndex = 18;
            this.cb_ms_zone.SelectedIndexChanged += new System.EventHandler(this.cb_ms_zone_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(361, 16);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 13);
            this.label31.TabIndex = 17;
            this.label31.Text = "Zone:";
            // 
            // cb_ms_map
            // 
            this.cb_ms_map.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_ms_map.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_ms_map.FormattingEnabled = true;
            this.cb_ms_map.Location = new System.Drawing.Point(397, 40);
            this.cb_ms_map.Name = "cb_ms_map";
            this.cb_ms_map.Size = new System.Drawing.Size(90, 21);
            this.cb_ms_map.TabIndex = 16;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(365, 43);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(31, 13);
            this.label32.TabIndex = 15;
            this.label32.Text = "Map:";
            // 
            // chk_ms
            // 
            this.chk_ms.AutoSize = true;
            this.chk_ms.Location = new System.Drawing.Point(211, 29);
            this.chk_ms.Name = "chk_ms";
            this.chk_ms.Size = new System.Drawing.Size(116, 17);
            this.chk_ms.TabIndex = 14;
            this.chk_ms.Text = "Both same element";
            this.chk_ms.UseVisualStyleBackColor = true;
            this.chk_ms.Visible = false;
            // 
            // cb_ms_mt
            // 
            this.cb_ms_mt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_ms_mt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_ms_mt.BackColor = System.Drawing.SystemColors.Window;
            this.cb_ms_mt.FormattingEnabled = true;
            this.cb_ms_mt.Location = new System.Drawing.Point(88, 13);
            this.cb_ms_mt.Name = "cb_ms_mt";
            this.cb_ms_mt.Size = new System.Drawing.Size(90, 21);
            this.cb_ms_mt.TabIndex = 13;
            this.cb_ms_mt.SelectedIndexChanged += new System.EventHandler(this.cb_ms_mt_SelectedIndexChanged);
            this.cb_ms_mt.Leave += new System.EventHandler(this.cb_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Monster Type:";
            // 
            // cb_ms_mn
            // 
            this.cb_ms_mn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_ms_mn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_ms_mn.FormattingEnabled = true;
            this.cb_ms_mn.Location = new System.Drawing.Point(88, 40);
            this.cb_ms_mn.Name = "cb_ms_mn";
            this.cb_ms_mn.Size = new System.Drawing.Size(90, 21);
            this.cb_ms_mn.TabIndex = 11;
            this.cb_ms_mn.SelectedIndexChanged += new System.EventHandler(this.cb_ms_mn_SelectedIndexChanged);
            this.cb_ms_mn.Leave += new System.EventHandler(this.cb_Leave);
            // 
            // btn_ms
            // 
            this.btn_ms.Location = new System.Drawing.Point(45, 93);
            this.btn_ms.Name = "btn_ms";
            this.btn_ms.Size = new System.Drawing.Size(75, 23);
            this.btn_ms.TabIndex = 10;
            this.btn_ms.Text = "Search";
            this.btn_ms.UseVisualStyleBackColor = true;
            this.btn_ms.Click += new System.EventHandler(this.btn_ms_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Monster Name:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.chk_tl_next);
            this.tabPage2.Controls.Add(this.tb_tl_next);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.chk_tl_dex);
            this.tabPage2.Controls.Add(this.tb_tl_dex);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.tb_tl_def);
            this.tabPage2.Controls.Add(this.btn_tl);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(519, 137);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Player Leveling";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // chk_tl_next
            // 
            this.chk_tl_next.AutoSize = true;
            this.chk_tl_next.Location = new System.Drawing.Point(380, 62);
            this.chk_tl_next.Name = "chk_tl_next";
            this.chk_tl_next.Size = new System.Drawing.Size(123, 17);
            this.chk_tl_next.TabIndex = 20;
            this.chk_tl_next.Text = "Check for next spots";
            this.chk_tl_next.UseVisualStyleBackColor = true;
            this.chk_tl_next.CheckedChanged += new System.EventHandler(this.chk_tl_next_CheckedChanged);
            // 
            // tb_tl_next
            // 
            this.tb_tl_next.Enabled = false;
            this.tb_tl_next.Location = new System.Drawing.Point(465, 36);
            this.tb_tl_next.MaxLength = 2;
            this.tb_tl_next.Name = "tb_tl_next";
            this.tb_tl_next.Size = new System.Drawing.Size(29, 20);
            this.tb_tl_next.TabIndex = 19;
            this.tb_tl_next.Text = "1";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(377, 39);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(87, 13);
            this.label20.TabIndex = 18;
            this.label20.Text = "Number of spots:";
            // 
            // chk_tl_dex
            // 
            this.chk_tl_dex.AutoSize = true;
            this.chk_tl_dex.Location = new System.Drawing.Point(197, 62);
            this.chk_tl_dex.Name = "chk_tl_dex";
            this.chk_tl_dex.Size = new System.Drawing.Size(159, 17);
            this.chk_tl_dex.TabIndex = 11;
            this.chk_tl_dex.Text = "Use Dexterity in calculations";
            this.chk_tl_dex.UseVisualStyleBackColor = true;
            this.chk_tl_dex.CheckedChanged += new System.EventHandler(this.chk_tl_dex_CheckedChanged);
            // 
            // tb_tl_dex
            // 
            this.tb_tl_dex.Enabled = false;
            this.tb_tl_dex.Location = new System.Drawing.Point(277, 36);
            this.tb_tl_dex.MaxLength = 9;
            this.tb_tl_dex.Name = "tb_tl_dex";
            this.tb_tl_dex.Size = new System.Drawing.Size(70, 20);
            this.tb_tl_dex.TabIndex = 10;
            this.tb_tl_dex.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(194, 39);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "Player Dexterity:";
            // 
            // tb_tl_def
            // 
            this.tb_tl_def.Location = new System.Drawing.Point(91, 36);
            this.tb_tl_def.MaxLength = 9;
            this.tb_tl_def.Name = "tb_tl_def";
            this.tb_tl_def.Size = new System.Drawing.Size(70, 20);
            this.tb_tl_def.TabIndex = 8;
            this.tb_tl_def.Text = "0";
            // 
            // btn_tl
            // 
            this.btn_tl.Location = new System.Drawing.Point(45, 93);
            this.btn_tl.Name = "btn_tl";
            this.btn_tl.Size = new System.Drawing.Size(75, 23);
            this.btn_tl.TabIndex = 7;
            this.btn_tl.Text = "Search";
            this.btn_tl.UseVisualStyleBackColor = true;
            this.btn_tl.Click += new System.EventHandler(this.btn_tl_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Player Defense:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.cb_pl_type);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.chk_pl_next);
            this.tabPage1.Controls.Add(this.tb_pl_next);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.chk_pl_dex);
            this.tabPage1.Controls.Add(this.tb_pl_dex);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.tb_pl_def);
            this.tabPage1.Controls.Add(this.cb_pl);
            this.tabPage1.Controls.Add(this.btn_pl);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(519, 137);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Pet Leveling";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.chk_pl_element);
            this.panel2.Controls.Add(this.chk_pl_element_avoid);
            this.panel2.Location = new System.Drawing.Point(178, 65);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(151, 67);
            this.panel2.TabIndex = 23;
            // 
            // chk_pl_element
            // 
            this.chk_pl_element.AutoSize = true;
            this.chk_pl_element.Location = new System.Drawing.Point(3, 11);
            this.chk_pl_element.Name = "chk_pl_element";
            this.chk_pl_element.Size = new System.Drawing.Size(149, 17);
            this.chk_pl_element.TabIndex = 3;
            this.chk_pl_element.Text = "Only elemental advantage";
            this.chk_pl_element.UseVisualStyleBackColor = true;
            this.chk_pl_element.CheckedChanged += new System.EventHandler(this.chk_pl_element_CheckedChanged);
            // 
            // chk_pl_element_avoid
            // 
            this.chk_pl_element_avoid.AutoSize = true;
            this.chk_pl_element_avoid.Location = new System.Drawing.Point(3, 38);
            this.chk_pl_element_avoid.Name = "chk_pl_element_avoid";
            this.chk_pl_element_avoid.Size = new System.Drawing.Size(134, 17);
            this.chk_pl_element_avoid.TabIndex = 23;
            this.chk_pl_element_avoid.Text = "Avoid stronger element";
            this.chk_pl_element_avoid.UseVisualStyleBackColor = true;
            this.chk_pl_element_avoid.CheckedChanged += new System.EventHandler(this.chk_pl_element_avoid_CheckedChanged);
            // 
            // cb_pl_type
            // 
            this.cb_pl_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_pl_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_pl_type.FormattingEnabled = true;
            this.cb_pl_type.Items.AddRange(new object[] {
            "Non-Evo",
            "Evo",
            "Super"});
            this.cb_pl_type.Location = new System.Drawing.Point(78, 60);
            this.cb_pl_type.Name = "cb_pl_type";
            this.cb_pl_type.Size = new System.Drawing.Size(70, 21);
            this.cb_pl_type.TabIndex = 25;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(24, 63);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 13);
            this.label29.TabIndex = 24;
            this.label29.Text = "Pet Type:";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radioButtonColorEffective);
            this.panel1.Controls.Add(this.radioButtonColorElement);
            this.panel1.Controls.Add(this.radioButtonColorNone);
            this.panel1.Location = new System.Drawing.Point(415, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(98, 73);
            this.panel1.TabIndex = 22;
            // 
            // radioButtonColorEffective
            // 
            this.radioButtonColorEffective.AutoSize = true;
            this.radioButtonColorEffective.Location = new System.Drawing.Point(6, 49);
            this.radioButtonColorEffective.Name = "radioButtonColorEffective";
            this.radioButtonColorEffective.Size = new System.Drawing.Size(89, 17);
            this.radioButtonColorEffective.TabIndex = 2;
            this.radioButtonColorEffective.Text = "Effectiveness";
            this.radioButtonColorEffective.UseVisualStyleBackColor = true;
            // 
            // radioButtonColorElement
            // 
            this.radioButtonColorElement.AutoSize = true;
            this.radioButtonColorElement.Location = new System.Drawing.Point(6, 27);
            this.radioButtonColorElement.Name = "radioButtonColorElement";
            this.radioButtonColorElement.Size = new System.Drawing.Size(63, 17);
            this.radioButtonColorElement.TabIndex = 1;
            this.radioButtonColorElement.Text = "Element";
            this.radioButtonColorElement.UseVisualStyleBackColor = true;
            // 
            // radioButtonColorNone
            // 
            this.radioButtonColorNone.AutoSize = true;
            this.radioButtonColorNone.Checked = true;
            this.radioButtonColorNone.Location = new System.Drawing.Point(6, 5);
            this.radioButtonColorNone.Name = "radioButtonColorNone";
            this.radioButtonColorNone.Size = new System.Drawing.Size(51, 17);
            this.radioButtonColorNone.TabIndex = 0;
            this.radioButtonColorNone.TabStop = true;
            this.radioButtonColorNone.Text = "None";
            this.radioButtonColorNone.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(344, 90);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 13);
            this.label21.TabIndex = 21;
            this.label21.Text = "Color cells by";
            // 
            // chk_pl_next
            // 
            this.chk_pl_next.AutoSize = true;
            this.chk_pl_next.Location = new System.Drawing.Point(373, 38);
            this.chk_pl_next.Name = "chk_pl_next";
            this.chk_pl_next.Size = new System.Drawing.Size(123, 17);
            this.chk_pl_next.TabIndex = 17;
            this.chk_pl_next.Text = "Check for next spots";
            this.chk_pl_next.UseVisualStyleBackColor = true;
            this.chk_pl_next.CheckedChanged += new System.EventHandler(this.chk_pl_next_CheckedChanged);
            // 
            // tb_pl_next
            // 
            this.tb_pl_next.Enabled = false;
            this.tb_pl_next.Location = new System.Drawing.Point(458, 13);
            this.tb_pl_next.MaxLength = 2;
            this.tb_pl_next.Name = "tb_pl_next";
            this.tb_pl_next.Size = new System.Drawing.Size(29, 20);
            this.tb_pl_next.TabIndex = 16;
            this.tb_pl_next.Text = "1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(370, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(87, 13);
            this.label19.TabIndex = 15;
            this.label19.Text = "Number of spots:";
            // 
            // chk_pl_dex
            // 
            this.chk_pl_dex.AutoSize = true;
            this.chk_pl_dex.Location = new System.Drawing.Point(178, 38);
            this.chk_pl_dex.Name = "chk_pl_dex";
            this.chk_pl_dex.Size = new System.Drawing.Size(159, 17);
            this.chk_pl_dex.TabIndex = 14;
            this.chk_pl_dex.Text = "Use Dexterity in calculations";
            this.chk_pl_dex.UseVisualStyleBackColor = true;
            this.chk_pl_dex.CheckedChanged += new System.EventHandler(this.chk_pl_dex_CheckedChanged);
            // 
            // tb_pl_dex
            // 
            this.tb_pl_dex.Enabled = false;
            this.tb_pl_dex.Location = new System.Drawing.Point(247, 13);
            this.tb_pl_dex.MaxLength = 9;
            this.tb_pl_dex.Name = "tb_pl_dex";
            this.tb_pl_dex.Size = new System.Drawing.Size(70, 20);
            this.tb_pl_dex.TabIndex = 13;
            this.tb_pl_dex.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(175, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 13);
            this.label18.TabIndex = 12;
            this.label18.Text = "Pet Dexterity:";
            // 
            // tb_pl_def
            // 
            this.tb_pl_def.Location = new System.Drawing.Point(78, 13);
            this.tb_pl_def.MaxLength = 9;
            this.tb_pl_def.Name = "tb_pl_def";
            this.tb_pl_def.Size = new System.Drawing.Size(70, 20);
            this.tb_pl_def.TabIndex = 5;
            this.tb_pl_def.Text = "0";
            // 
            // cb_pl
            // 
            this.cb_pl.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_pl.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_pl.FormattingEnabled = true;
            this.cb_pl.Items.AddRange(new object[] {
            "Water",
            "Fire",
            "Metal",
            "Wood",
            "Earth"});
            this.cb_pl.Location = new System.Drawing.Point(78, 36);
            this.cb_pl.Name = "cb_pl";
            this.cb_pl.Size = new System.Drawing.Size(70, 21);
            this.cb_pl.TabIndex = 4;
            this.cb_pl.Leave += new System.EventHandler(this.cb_Leave);
            // 
            // btn_pl
            // 
            this.btn_pl.Location = new System.Drawing.Point(45, 93);
            this.btn_pl.Name = "btn_pl";
            this.btn_pl.Size = new System.Drawing.Size(75, 23);
            this.btn_pl.TabIndex = 2;
            this.btn_pl.Text = "Search";
            this.btn_pl.UseVisualStyleBackColor = true;
            this.btn_pl.Click += new System.EventHandler(this.btn_pl_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Pet Element:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pet Defense:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(0, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(527, 163);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tb_sc_dexteritypercentage);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.tb_sc_defensepercentage);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.tb_sc_attackpercentage);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.tb_sc_lifegr);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.tb_sc_attgr);
            this.tabPage4.Controls.Add(this.tb_sc_life);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.tb_sc_dexterity);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.tb_sc_defense);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.tb_sc_attack);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.tb_sc_level);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.cb_sc);
            this.tabPage4.Controls.Add(this.btn_sc);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(519, 137);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Stats Calculator";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tb_sc_dexteritypercentage
            // 
            this.tb_sc_dexteritypercentage.Location = new System.Drawing.Point(397, 59);
            this.tb_sc_dexteritypercentage.MaxLength = 5;
            this.tb_sc_dexteritypercentage.Name = "tb_sc_dexteritypercentage";
            this.tb_sc_dexteritypercentage.Size = new System.Drawing.Size(34, 20);
            this.tb_sc_dexteritypercentage.TabIndex = 32;
            this.tb_sc_dexteritypercentage.Text = "0";
            this.tb_sc_dexteritypercentage.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(357, 62);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "DEX%:";
            this.label16.Visible = false;
            // 
            // tb_sc_defensepercentage
            // 
            this.tb_sc_defensepercentage.Location = new System.Drawing.Point(397, 36);
            this.tb_sc_defensepercentage.MaxLength = 5;
            this.tb_sc_defensepercentage.Name = "tb_sc_defensepercentage";
            this.tb_sc_defensepercentage.Size = new System.Drawing.Size(34, 20);
            this.tb_sc_defensepercentage.TabIndex = 30;
            this.tb_sc_defensepercentage.Text = "0";
            this.tb_sc_defensepercentage.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(358, 39);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "DEF%:";
            this.label15.Visible = false;
            // 
            // tb_sc_attackpercentage
            // 
            this.tb_sc_attackpercentage.Location = new System.Drawing.Point(397, 13);
            this.tb_sc_attackpercentage.MaxLength = 5;
            this.tb_sc_attackpercentage.Name = "tb_sc_attackpercentage";
            this.tb_sc_attackpercentage.Size = new System.Drawing.Size(34, 20);
            this.tb_sc_attackpercentage.TabIndex = 28;
            this.tb_sc_attackpercentage.Text = "0";
            this.tb_sc_attackpercentage.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(358, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "ATK%:";
            this.label14.Visible = false;
            // 
            // tb_sc_lifegr
            // 
            this.tb_sc_lifegr.Location = new System.Drawing.Point(384, 111);
            this.tb_sc_lifegr.MaxLength = 9;
            this.tb_sc_lifegr.Name = "tb_sc_lifegr";
            this.tb_sc_lifegr.Size = new System.Drawing.Size(36, 20);
            this.tb_sc_lifegr.TabIndex = 26;
            this.tb_sc_lifegr.Text = "0.0";
            this.tb_sc_lifegr.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(337, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Life GR:";
            this.label13.Visible = false;
            // 
            // tb_sc_attgr
            // 
            this.tb_sc_attgr.Location = new System.Drawing.Point(299, 111);
            this.tb_sc_attgr.MaxLength = 9;
            this.tb_sc_attgr.Name = "tb_sc_attgr";
            this.tb_sc_attgr.Size = new System.Drawing.Size(36, 20);
            this.tb_sc_attgr.TabIndex = 18;
            this.tb_sc_attgr.Text = "0.0";
            this.tb_sc_attgr.Visible = false;
            // 
            // tb_sc_life
            // 
            this.tb_sc_life.Location = new System.Drawing.Point(267, 82);
            this.tb_sc_life.MaxLength = 9;
            this.tb_sc_life.Name = "tb_sc_life";
            this.tb_sc_life.ReadOnly = true;
            this.tb_sc_life.Size = new System.Drawing.Size(70, 20);
            this.tb_sc_life.TabIndex = 24;
            this.tb_sc_life.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(248, 114);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "3-att GR:";
            this.label12.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(234, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Life:";
            // 
            // tb_sc_dexterity
            // 
            this.tb_sc_dexterity.Location = new System.Drawing.Point(267, 59);
            this.tb_sc_dexterity.MaxLength = 9;
            this.tb_sc_dexterity.Name = "tb_sc_dexterity";
            this.tb_sc_dexterity.ReadOnly = true;
            this.tb_sc_dexterity.Size = new System.Drawing.Size(70, 20);
            this.tb_sc_dexterity.TabIndex = 22;
            this.tb_sc_dexterity.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(211, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Dexterity:";
            // 
            // tb_sc_defense
            // 
            this.tb_sc_defense.Location = new System.Drawing.Point(267, 36);
            this.tb_sc_defense.MaxLength = 9;
            this.tb_sc_defense.Name = "tb_sc_defense";
            this.tb_sc_defense.ReadOnly = true;
            this.tb_sc_defense.Size = new System.Drawing.Size(70, 20);
            this.tb_sc_defense.TabIndex = 20;
            this.tb_sc_defense.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(211, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Defense:";
            // 
            // tb_sc_attack
            // 
            this.tb_sc_attack.Location = new System.Drawing.Point(267, 13);
            this.tb_sc_attack.MaxLength = 9;
            this.tb_sc_attack.Name = "tb_sc_attack";
            this.tb_sc_attack.ReadOnly = true;
            this.tb_sc_attack.Size = new System.Drawing.Size(70, 20);
            this.tb_sc_attack.TabIndex = 18;
            this.tb_sc_attack.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(220, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Attack:";
            // 
            // tb_sc_level
            // 
            this.tb_sc_level.Location = new System.Drawing.Point(86, 40);
            this.tb_sc_level.MaxLength = 9;
            this.tb_sc_level.Name = "tb_sc_level";
            this.tb_sc_level.Size = new System.Drawing.Size(70, 20);
            this.tb_sc_level.TabIndex = 16;
            this.tb_sc_level.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Monster Level:";
            // 
            // cb_sc
            // 
            this.cb_sc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_sc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_sc.FormattingEnabled = true;
            this.cb_sc.Location = new System.Drawing.Point(86, 13);
            this.cb_sc.Name = "cb_sc";
            this.cb_sc.Size = new System.Drawing.Size(90, 21);
            this.cb_sc.TabIndex = 14;
            this.cb_sc.SelectionChangeCommitted += new System.EventHandler(this.cb_sc_SelectionChangeCommitted);
            this.cb_sc.Leave += new System.EventHandler(this.cb_Leave);
            // 
            // btn_sc
            // 
            this.btn_sc.Location = new System.Drawing.Point(45, 93);
            this.btn_sc.Name = "btn_sc";
            this.btn_sc.Size = new System.Drawing.Size(75, 23);
            this.btn_sc.TabIndex = 13;
            this.btn_sc.Text = "Calculate";
            this.btn_sc.UseVisualStyleBackColor = true;
            this.btn_sc.Click += new System.EventHandler(this.btn_sc_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Monster Name:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.cb_passes_type);
            this.tabPage5.Controls.Add(this.label30);
            this.tabPage5.Controls.Add(this.cb_passes_p);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.chk_passes_dex);
            this.tabPage5.Controls.Add(this.tb_passes_pdex);
            this.tabPage5.Controls.Add(this.label25);
            this.tabPage5.Controls.Add(this.tb_passes_pdef);
            this.tabPage5.Controls.Add(this.cb_passes_el);
            this.tabPage5.Controls.Add(this.label26);
            this.tabPage5.Controls.Add(this.label27);
            this.tabPage5.Controls.Add(this.btn_passes);
            this.tabPage5.Controls.Add(this.chk_passes_def);
            this.tabPage5.Controls.Add(this.tb_passes_tdef);
            this.tabPage5.Controls.Add(this.label24);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(519, 137);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Passes";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // cb_passes_type
            // 
            this.cb_passes_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_passes_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_passes_type.FormattingEnabled = true;
            this.cb_passes_type.Items.AddRange(new object[] {
            "Non-Evo",
            "Evo",
            "Super"});
            this.cb_passes_type.Location = new System.Drawing.Point(418, 36);
            this.cb_passes_type.Name = "cb_passes_type";
            this.cb_passes_type.Size = new System.Drawing.Size(70, 21);
            this.cb_passes_type.TabIndex = 27;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(363, 39);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(53, 13);
            this.label30.TabIndex = 26;
            this.label30.Text = "Pet Type:";
            // 
            // cb_passes_p
            // 
            this.cb_passes_p.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_passes_p.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_passes_p.FormattingEnabled = true;
            this.cb_passes_p.Items.AddRange(new object[] {
            "Sky Pass",
            "Friend Pass",
            "Couple Pass"});
            this.cb_passes_p.Location = new System.Drawing.Point(52, 12);
            this.cb_passes_p.Name = "cb_passes_p";
            this.cb_passes_p.Size = new System.Drawing.Size(82, 21);
            this.cb_passes_p.TabIndex = 24;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(17, 15);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(33, 13);
            this.label28.TabIndex = 23;
            this.label28.Text = "Pass:";
            // 
            // chk_passes_dex
            // 
            this.chk_passes_dex.AutoSize = true;
            this.chk_passes_dex.Location = new System.Drawing.Point(348, 111);
            this.chk_passes_dex.Name = "chk_passes_dex";
            this.chk_passes_dex.Size = new System.Drawing.Size(159, 17);
            this.chk_passes_dex.TabIndex = 22;
            this.chk_passes_dex.Text = "Use Dexterity in calculations";
            this.chk_passes_dex.UseVisualStyleBackColor = true;
            this.chk_passes_dex.CheckedChanged += new System.EventHandler(this.chk_passes_dex_CheckedChanged);
            // 
            // tb_passes_pdex
            // 
            this.tb_passes_pdex.Enabled = false;
            this.tb_passes_pdex.Location = new System.Drawing.Point(418, 85);
            this.tb_passes_pdex.MaxLength = 9;
            this.tb_passes_pdex.Name = "tb_passes_pdex";
            this.tb_passes_pdex.Size = new System.Drawing.Size(70, 20);
            this.tb_passes_pdex.TabIndex = 21;
            this.tb_passes_pdex.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(346, 88);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 13);
            this.label25.TabIndex = 20;
            this.label25.Text = "Pet Dexterity:";
            // 
            // tb_passes_pdef
            // 
            this.tb_passes_pdef.Location = new System.Drawing.Point(418, 61);
            this.tb_passes_pdef.MaxLength = 9;
            this.tb_passes_pdef.Name = "tb_passes_pdef";
            this.tb_passes_pdef.Size = new System.Drawing.Size(70, 20);
            this.tb_passes_pdef.TabIndex = 19;
            this.tb_passes_pdef.Text = "0";
            // 
            // cb_passes_el
            // 
            this.cb_passes_el.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_passes_el.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_passes_el.FormattingEnabled = true;
            this.cb_passes_el.Items.AddRange(new object[] {
            "Water",
            "Fire",
            "Metal",
            "Wood",
            "Earth"});
            this.cb_passes_el.Location = new System.Drawing.Point(418, 11);
            this.cb_passes_el.Name = "cb_passes_el";
            this.cb_passes_el.Size = new System.Drawing.Size(70, 21);
            this.cb_passes_el.TabIndex = 18;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(349, 14);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 13);
            this.label26.TabIndex = 17;
            this.label26.Text = "Pet Element:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(347, 64);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(69, 13);
            this.label27.TabIndex = 16;
            this.label27.Text = "Pet Defense:";
            // 
            // btn_passes
            // 
            this.btn_passes.Location = new System.Drawing.Point(45, 93);
            this.btn_passes.Name = "btn_passes";
            this.btn_passes.Size = new System.Drawing.Size(75, 23);
            this.btn_passes.TabIndex = 15;
            this.btn_passes.Text = "Calculate";
            this.btn_passes.UseVisualStyleBackColor = true;
            this.btn_passes.Click += new System.EventHandler(this.btn_passes_Click);
            // 
            // chk_passes_def
            // 
            this.chk_passes_def.AutoSize = true;
            this.chk_passes_def.Location = new System.Drawing.Point(180, 41);
            this.chk_passes_def.Name = "chk_passes_def";
            this.chk_passes_def.Size = new System.Drawing.Size(61, 17);
            this.chk_passes_def.TabIndex = 14;
            this.chk_passes_def.Text = "Defend";
            this.chk_passes_def.UseVisualStyleBackColor = true;
            // 
            // tb_passes_tdef
            // 
            this.tb_passes_tdef.Location = new System.Drawing.Point(260, 12);
            this.tb_passes_tdef.MaxLength = 9;
            this.tb_passes_tdef.Name = "tb_passes_tdef";
            this.tb_passes_tdef.Size = new System.Drawing.Size(70, 20);
            this.tb_passes_tdef.TabIndex = 13;
            this.tb_passes_tdef.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(177, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 13);
            this.label24.TabIndex = 12;
            this.label24.Text = "Player Defense:";
            // 
            // linkLabelReleases
            // 
            this.linkLabelReleases.AutoSize = true;
            this.linkLabelReleases.Location = new System.Drawing.Point(553, 83);
            this.linkLabelReleases.Name = "linkLabelReleases";
            this.linkLabelReleases.Size = new System.Drawing.Size(71, 13);
            this.linkLabelReleases.TabIndex = 3;
            this.linkLabelReleases.TabStop = true;
            this.linkLabelReleases.Text = "New releases";
            this.linkLabelReleases.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelReleases_LinkClicked);
            // 
            // linkLabelSource
            // 
            this.linkLabelSource.AutoSize = true;
            this.linkLabelSource.Location = new System.Drawing.Point(652, 83);
            this.linkLabelSource.Name = "linkLabelSource";
            this.linkLabelSource.Size = new System.Drawing.Size(68, 13);
            this.linkLabelSource.TabIndex = 4;
            this.linkLabelSource.TabStop = true;
            this.linkLabelSource.Text = "Source code";
            this.linkLabelSource.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelSource_LinkClicked);
            // 
            // linkLabelIssue
            // 
            this.linkLabelIssue.AutoSize = true;
            this.linkLabelIssue.Location = new System.Drawing.Point(598, 132);
            this.linkLabelIssue.Name = "linkLabelIssue";
            this.linkLabelIssue.Size = new System.Drawing.Size(79, 13);
            this.linkLabelIssue.TabIndex = 5;
            this.linkLabelIssue.TabStop = true;
            this.linkLabelIssue.Text = "Make an issue!";
            this.linkLabelIssue.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelIssue_LinkClicked);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(564, 112);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(145, 13);
            this.label23.TabIndex = 6;
            this.label23.Text = "Ideas, Suggestions or Bugs? ";
            // 
            // dgv_passes
            // 
            this.dgv_passes.AllowUserToAddRows = false;
            this.dgv_passes.AllowUserToDeleteRows = false;
            this.dgv_passes.AllowUserToOrderColumns = true;
            this.dgv_passes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_passes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_passes_floor,
            this.dgv_passes_name,
            this.dgv_passes_level,
            this.dgv_passes_attack,
            this.dgv_passes_defense,
            this.dgv_passes_dexterity,
            this.dgv_passes_life,
            this.dgv_passes_element,
            this.dgv_passes_notes});
            this.dgv_passes.Location = new System.Drawing.Point(4, 163);
            this.dgv_passes.Name = "dgv_passes";
            this.dgv_passes.ReadOnly = true;
            this.dgv_passes.RowHeadersVisible = false;
            this.dgv_passes.Size = new System.Drawing.Size(716, 171);
            this.dgv_passes.TabIndex = 7;
            this.dgv_passes.Visible = false;
            this.dgv_passes.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.dgv_passes_SortCompare);
            // 
            // dgv_passes_floor
            // 
            this.dgv_passes_floor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_passes_floor.HeaderText = "Floor";
            this.dgv_passes_floor.Name = "dgv_passes_floor";
            this.dgv_passes_floor.ReadOnly = true;
            this.dgv_passes_floor.Width = 55;
            // 
            // dgv_passes_name
            // 
            this.dgv_passes_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_passes_name.HeaderText = "Name";
            this.dgv_passes_name.Name = "dgv_passes_name";
            this.dgv_passes_name.ReadOnly = true;
            this.dgv_passes_name.Width = 60;
            // 
            // dgv_passes_level
            // 
            this.dgv_passes_level.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_passes_level.HeaderText = "Level";
            this.dgv_passes_level.Name = "dgv_passes_level";
            this.dgv_passes_level.ReadOnly = true;
            this.dgv_passes_level.Width = 58;
            // 
            // dgv_passes_attack
            // 
            this.dgv_passes_attack.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_passes_attack.HeaderText = "Attack";
            this.dgv_passes_attack.Name = "dgv_passes_attack";
            this.dgv_passes_attack.ReadOnly = true;
            this.dgv_passes_attack.Width = 63;
            // 
            // dgv_passes_defense
            // 
            this.dgv_passes_defense.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_passes_defense.HeaderText = "Defense";
            this.dgv_passes_defense.Name = "dgv_passes_defense";
            this.dgv_passes_defense.ReadOnly = true;
            this.dgv_passes_defense.Width = 72;
            // 
            // dgv_passes_dexterity
            // 
            this.dgv_passes_dexterity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_passes_dexterity.HeaderText = "Dexterity";
            this.dgv_passes_dexterity.Name = "dgv_passes_dexterity";
            this.dgv_passes_dexterity.ReadOnly = true;
            this.dgv_passes_dexterity.Width = 73;
            // 
            // dgv_passes_life
            // 
            this.dgv_passes_life.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_passes_life.HeaderText = "Life";
            this.dgv_passes_life.Name = "dgv_passes_life";
            this.dgv_passes_life.ReadOnly = true;
            this.dgv_passes_life.Width = 49;
            // 
            // dgv_passes_element
            // 
            this.dgv_passes_element.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_passes_element.HeaderText = "Element";
            this.dgv_passes_element.Name = "dgv_passes_element";
            this.dgv_passes_element.ReadOnly = true;
            this.dgv_passes_element.Width = 70;
            // 
            // dgv_passes_notes
            // 
            this.dgv_passes_notes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dgv_passes_notes.HeaderText = "Notes";
            this.dgv_passes_notes.Name = "dgv_passes_notes";
            this.dgv_passes_notes.ReadOnly = true;
            this.dgv_passes_notes.Width = 60;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 352);
            this.Controls.Add(this.dgv_encounters);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.linkLabelIssue);
            this.Controls.Add(this.linkLabelSource);
            this.Controls.Add(this.linkLabelReleases);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_passes);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Monster&Me Monster Kit Reborn";
            this.SizeChanged += new System.EventHandler(this.Form_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_encounters)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_passes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgv_encounters;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ComboBox cb_ms_mn;
        private System.Windows.Forms.Button btn_ms;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox tb_tl_def;
        private System.Windows.Forms.Button btn_tl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox tb_pl_def;
        private System.Windows.Forms.ComboBox cb_pl;
        private System.Windows.Forms.CheckBox chk_pl_element;
        private System.Windows.Forms.Button btn_pl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox tb_sc_dexteritypercentage;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb_sc_defensepercentage;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tb_sc_attackpercentage;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb_sc_lifegr;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_sc_attgr;
        private System.Windows.Forms.TextBox tb_sc_life;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_sc_dexterity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_sc_defense;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_sc_attack;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_sc_level;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cb_sc;
        private System.Windows.Forms.Button btn_sc;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_tl_dex;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chk_tl_dex;
        private System.Windows.Forms.CheckBox chk_pl_dex;
        private System.Windows.Forms.TextBox tb_pl_dex;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chk_tl_next;
        private System.Windows.Forms.TextBox tb_tl_next;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox chk_pl_next;
        private System.Windows.Forms.TextBox tb_pl_next;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonColorEffective;
        private System.Windows.Forms.RadioButton radioButtonColorElement;
        private System.Windows.Forms.RadioButton radioButtonColorNone;
        private System.Windows.Forms.ComboBox cb_ms_mt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.LinkLabel linkLabelReleases;
        private System.Windows.Forms.LinkLabel linkLabelSource;
        private System.Windows.Forms.LinkLabel linkLabelIssue;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox tb_passes_pdef;
        private System.Windows.Forms.ComboBox cb_passes_el;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btn_passes;
        private System.Windows.Forms.CheckBox chk_passes_def;
        private System.Windows.Forms.TextBox tb_passes_tdef;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox chk_passes_dex;
        private System.Windows.Forms.TextBox tb_passes_pdex;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DataGridView dgv_passes;
        private System.Windows.Forms.ComboBox cb_passes_p;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox chk_ms;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_map;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_coords;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m1name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m1level;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m1attack;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m1defense;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m1dexterity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m1life;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m1element;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m2name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m2level;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m2attack;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m2defense;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m2dexterity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m2life;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_encounters_m2element;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_passes_floor;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_passes_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_passes_level;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_passes_attack;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_passes_defense;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_passes_dexterity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_passes_life;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_passes_element;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_passes_notes;
        private System.Windows.Forms.CheckBox chk_pl_element_avoid;
        private System.Windows.Forms.ComboBox cb_pl_type;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cb_passes_type;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cb_ms_zone;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cb_ms_map;
        private System.Windows.Forms.Label label32;
    }
}

